<?php

use Spatie\Permission\PermissionServiceProvider;

require_once __DIR__.'/../vendor/autoload.php';

(new Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(
    dirname(__DIR__)
))->bootstrap();

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    dirname(__DIR__)
);

$app->withFacades();

$app->withEloquent();

if(!class_exists('DingoApi'))
{
    class_alias('Dingo\Api\Facade\API','DingoApi');
}
if(!class_exists('DingoRoute'))
{
    class_alias('Dingo\Api\Facade\Route','DingoRoute');
}
// class_alias('Illuminate\Support\Facades\Storage', 'Storage');

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);
$app->configure('cors');
$app->configure('constants');
$app->configure('database');
$app->configure('graphql');
$app->configure('validations');
$app->configure('permission');
$app->configure('credentials');
$app->configure('auth'); // Passport
$app->configure('filesystems');
/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
   \Barryvdh\Cors\HandleCors::class,
]);
$app->routeMiddleware([
    'cors' => App\Http\Middleware\CorsMiddleware::class,
    'auth'       => App\Http\Middleware\Authenticate::class,
    'permission' => Spatie\Permission\Middlewares\PermissionMiddleware::class,
    'role'       => Spatie\Permission\Middlewares\RoleMiddleware::class,
]);

// $app->routeMiddleware([
//     'CorsMiddleware' => App\Http\Middleware\CorsMiddleware::class,
// ]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\CoreServiceProvider::class);
$app->register(Barryvdh\Cors\ServiceProvider::class);
$app->register(App\Providers\CoreServiceProvider::class);
// $app->register(App\Providers\CatchAllOptionsRequestsProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(Maatwebsite\Excel\ExcelServiceProvider::class);
$app->register(Illuminate\Redis\RedisServiceProvider::class);
$app->register(Dingo\Api\Provider\LumenServiceProvider::class); //Dingo
$app->register(Illuminate\Filesystem\FilesystemServiceProvider::class);
$app['Dingo\Api\Transformer\Factory']->setAdapter(function ($app) {
    return new Dingo\Api\Transformer\Adapter\Fractal(new League\Fractal\Manager, 'include', ',');
});
$app['Dingo\Api\Exception\Handler']->setErrorFormat([
    'message' => ':message',
    'errors' => ':errors',
    'code' => ':status_code',
]);
$app->register(Rebing\GraphQL\GraphQLLumenServiceProvider::class);
$app->register(Spatie\Permission\PermissionServiceProvider::class);
$app->register(Laravel\Passport\PassportServiceProvider::class); // Passport
$app->register(Dusterio\LumenPassport\PassportServiceProvider::class); // Passport
// $app->alias('Excel', Maatwebsite\Excel\Facades\Excel::class);
$app->alias('cache', \Illuminate\Cache\CacheManager::class);  // if you don't have this already
class_alias(Maatwebsite\Excel\Facades\Excel::class,'Excel');
Dusterio\LumenPassport\LumenPassport::routes($app->router, ['prefix' => 'api/oauth'] );
//Dusterio\LumenPassport\LumenPassport::routes( $app );
/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/


$app->routeMiddleware([
    'api' => App\Http\Middleware\ApiMiddleware::class,
    'auth' => App\Http\Middleware\Authenticate::class,
    'client' => \Laravel\Passport\Http\Middleware\CheckClientCredentials::class
]);



$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/web.php';
});

return $app;
