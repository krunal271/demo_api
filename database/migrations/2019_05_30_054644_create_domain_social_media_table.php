<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainSocialMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_social_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('domain_id')->unsigned();
            $table->string('facebook', 150)->nullable();
            $table->string('twitter', 150)->nullable();
            $table->string('linkedin', 150)->nullable();
            $table->string('instagram', 150)->nullable();
            $table->string('printrest', 150)->nullable();
            $table->string('google', 150)->nullable();
            $table->string('youtube', 150)->nullable();
            $table->enum('status', ['0', '1'])->default('1')->comment('0 => Inactive , 1 => Active');
            $table->timestamps();
            $table->softDeletes();

            if (env('DB_FOREIGN_KEY_CONSTRAINT') === true) {
                $table->foreign('domain_id')->references('id')->on('domains');
            }
            $table->index('id', 'domain_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_social_media');
    }
}
