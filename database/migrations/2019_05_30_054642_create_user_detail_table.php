<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->enum('user_type', ['1', '2', '3'])->comment('1 => customer , 2 => licensee , 3 => cms_admin');
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('email', 80)->nullable();
            $table->string('contact_number', 20)->nullable();
            $table->enum('status',['0','1'])->default('1')->comment('0 => Inactive , 1 => Active');
            $table->timestamps();
            $table->softDeletes();

            $table->index(['id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_detail');
    }
}
