<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('licensee_id');
            $table->string('domain_name', 150);
            $table->string('cms_name', 100);
            $table->enum('status',['0','1'])->default('1')->comment('0 => Inactive , 1 => Active');
            $table->timestamps();
            $table->softDeletes();

            if(env('DB_FOREIGN_KEY_CONSTRAINT') === true) {
                $table->foreign('licensee_id')->references('id')->on('licensees');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domains');
    }
}
