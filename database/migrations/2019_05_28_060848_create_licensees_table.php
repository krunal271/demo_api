<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicenseesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licensees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('per_set_account_id')->unsigned();
            $table->string('business_name', 100);
            $table->string('business_email', 80);
            $table->string('business_phone_number', 20)->nullable();
            $table->string('apartment', 250)->nullable();
            $table->string('street', 100);
            $table->string('zip_code', 10);
            $table->string('city', 50);
            $table->string('state', 50);
            $table->integer('country_id');
            $table->integer('timezone_id')->default(0);
            $table->string('business_website_url', 250)->nullable();
            $table->enum('status', ['0', '1'])->default('1')->comment('0 => Inactive , 1 => Active');
            $table->timestamps();
            $table->softDeletes();

            if(env('DB_FOREIGN_KEY_CONSTRAINT') === true) {
                $table->foreign('per_set_account_id')->references('id')->on('pre_set_account_options');
            }
            $table->index(['id', 'per_set_account_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licensees');
    }
}
