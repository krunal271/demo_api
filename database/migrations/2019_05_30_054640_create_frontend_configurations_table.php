<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrontendConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frontend_configurations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('domain_id')->unsigned();
            $table->bigInteger('template_id')->unsigned();
            $table->string('logo', 250)->nullable();
            $table->string('favicon', 250)->nullable();
            $table->string('group_one_color', 20)->comment('Header');
            $table->string('group_two_color', 20)->comment('Footer');
            $table->string('group_three_color', 20)->comment('Search Panal Background');
            $table->string('page_title', 30)->comment('Page Title');
            $table->enum('b2b_view_status', ['0', '1'])->comment('0 => No , 1 => Yes');
            $table->timestamps();
            $table->softDeletes();


            if (env('DB_FOREIGN_KEY_CONSTRAINT') === true) {
                $table->foreign('template_id')->references('id')->on('templates');
            }
            $table->index(['id', 'template_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frontend_configurations');
    }
}
