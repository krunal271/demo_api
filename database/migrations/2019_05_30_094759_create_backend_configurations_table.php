<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackendConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backend_configurations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('domain_id')->unsigned();
            $table->bigInteger('backend_services_id')->unsigned();
            $table->enum('status',['0','1'])->default('1')->comment('0 => Inactive , 1 => Active');
            $table->timestamps();
            $table->softDeletes();


            if(env('DB_FOREIGN_KEY_CONSTRAINT') === true) {
                $table->foreign('domain_id')->references('id')->on('domains');
                $table->foreign('backend_services_id')->references('id')->on('backend_services');
            }
            $table->index(['id', 'domain_id','backend_services_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backend_configurations');
    }
}
