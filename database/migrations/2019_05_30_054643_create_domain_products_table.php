<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('domain_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->enum('status', ['0', '1'])->default('1')->comment('0 => Inactive , 1 => Active');
            $table->timestamps();
            $table->softDeletes();

            if (env('DB_FOREIGN_KEY_CONSTRAINT') === true) {
                $table->foreign('domain_id')->references('id')->on('domains');
                $table->foreign('product_id')->references('id')->on('products');
            }
            $table->index(['id', 'domain_id','product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_products');
    }
}
