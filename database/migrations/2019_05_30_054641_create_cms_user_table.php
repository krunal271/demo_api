<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('domain_id')->unsigned();
            $table->string('username', 80);
            $table->string('password', 150);
            $table->enum('status', ['0', '1'])->default('1')->comment('0 => Inactive ,1 => Active');
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->timestamps();
            $table->softDeletes();

            if(env('DB_FOREIGN_KEY_CONSTRAINT') === true) {
                $table->foreign('domain_id')->references('id')->on('domains');
            }
            $table->index(['id', 'domain_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_user');
    }
}
