<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('domain_id');
            $table->enum('region',['0','1'])->comment('0 => No , 1 => Yes');
            $table->enum('country',['0','1'])->comment('0 => No , 1 => Yes');
            $table->enum('city',['0','1'])->comment('0 => No , 1 => Yes');
            $table->timestamps();
            $table->softDeletes();

            if(env('DB_FOREIGN_KEY_CONSTRAINT') === true) {
                $table->foreign('domain_id')->references('id')->on('domains');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_data');
    }
}
