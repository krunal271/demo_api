<?php
use App\Models\CmsUser;
$factory->define('App\Models\Customer', function ($faker) {

    return [
        'customer_id' => $faker->randomNumber(4),
        'agent_id'=> CmsUser::all()->random()->id,
        'title' => 'Mr',
        'first_name'=>$faker->firstName,
        'last_name'=>$faker->lastName,
        'gender'=>'Male',
        'country_code'=>$faker->countryCode,
        'phone' => $faker->randomNumber(8),
        'email' => $faker->unique()->safeEmail,
        'contact_method' => 'Email',
        'domain' => 'rock-my-domain',
        'frequently_used_product' => 'Mobile',
        'accommodation_type' => '1 star',
        'room_type' => 'Smoking',
        'transport_type' => 'Flight',
        'cabin_class_type' => 'Economy',
        'seat_type' => 'Back',
        'meal_type' => 'Diabetic',
        'interests' => 'Book reading',
        'conversation_notes' => 'this is dummy conversation notes.',
        'middle_name' => 'Peterson',
        'nationality' => 'Albanian',
        'birth_date' => '1975-03-01',
        'passport_number' => 'FHSN5846QWE7',
        'passport_expiry_date' => '2022-03-01',
        'passport_issue_date' => '2020-03-01',
        'address_1' => $faker->address,
        'address_2' => $faker->secondaryAddress,
        'city' => $faker->city,
        'state' => $faker->country,
        'country' => $faker->countryCode,
        'postal_code' => $faker->postcode,
        'preferred_language' => 'English',

    ];
});
