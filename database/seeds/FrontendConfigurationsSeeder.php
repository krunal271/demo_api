<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FrontendConfigurationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $frontEndConfiguration = [
            [
                'template_id' => '1',
                'logo' => 'ooglelogo_color_272x92dp.png',
                'favicon' => 'googlelogo_color_272x92dp.png',
                'domain_id' => '1',
                'group_one_color' => '#FF5733',
                'group_two_color' => '#4D190E',
                'group_three_color' => '#FFBD00',
                'page_title' => 'Google',
                'b2b_view_status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'template_id' => '2',
                'logo' => 'googlelogo_color_272x92dp.png',
                'favicon' => 'googlelogo_color_272x92dp.png',
                'domain_id' => '2',
                'group_one_color' => '#063E42',
                'group_two_color' => '#118991',
                'group_three_color' => '#00F7FF',
                'page_title' => 'Google Plus',
                'b2b_view_status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        foreach ($frontEndConfiguration as $value) {
            DB::table('frontend_configurations')->insert($value);
        }
    }
}
