<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BackendServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $backendServices = [
            [
                'id' => 1,
                'parent_id' => '0',
                'name' => 'Content Management System',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 2,
                'parent_id' => '0',
                'name' => 'Analytice / Reporting',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 3,
                'parent_id' => '0',
                'name' => 'Booking Management',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 4,
                'parent_id' => '0',
                'name' => 'Account / CRM Management',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 5,
                'parent_id' => '0',
                'name' => 'User Management',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 6,
                'parent_id' => '0',
                'name' => 'Inventory MarketPlace',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 7,
                'parent_id' => '0',
                'name' => 'Setting',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 8,
                'parent_id' => '0',
                'name' => 'Support & FAQ',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 9,
                'parent_id' => '1',
                'name' => 'Consultant Dashboard',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 10,
                'parent_id' => '1',
                'name' => 'Product Manager Dashboard',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 11,
                'parent_id' => '1',
                'name' => 'Agent Dashboard',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 12,
                'parent_id' => '1',
                'name' => 'Administrator Dashboard',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 13,
                'parent_id' => '2',
                'name' => 'Google Analytics (Traffic)',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 14,
                'parent_id' => '2',
                'name' => 'Total Sales (Fully paid + Partially Paid Service)',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 15,
                'parent_id' => '2',
                'name' => 'Un-paid Sales (Balance on Partialy Paid Sales)',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 16,
                'parent_id' => '2',
                'name' => 'Un-paid Commisions',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 17,
                'parent_id' => '2',
                'name' => 'Paid Commissions',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 18,
                'parent_id' => '2',
                'name' => 'Agent Sales',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 19,
                'parent_id' => '2',
                'name' => 'Consultant Sales',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 20,
                'parent_id' => '2',
                'name' => 'Supplier Purchases',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 21,
                'parent_id' => '2',
                'name' => 'Un-Paid Suppliers',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 22,
                'parent_id' => '2',
                'name' => 'Paid Suppliers',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 23,
                'parent_id' => '3',
                'name' => 'Pending Bookings',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 24,
                'parent_id' => '3',
                'name' => 'Active Bookings',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 25,
                'parent_id' => '3',
                'name' => 'Archived Bookings',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 26,
                'parent_id' => '4',
                'name' => 'Pay Suppliers',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 27,
                'parent_id' => '4',
                'name' => 'Credit Note Suppliers',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 28,
                'parent_id' => '4',
                'name' => 'Refund Client (Consumer)',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 29,
                'parent_id' => '4',
                'name' => 'Write Off Balance',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 30,
                'parent_id' => '4',
                'name' => 'Re-Allocate Funds',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 31,
                'parent_id' => '4',
                'name' => 'Export Functionality',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 32,
                'parent_id' => '5',
                'name' => 'Licensee Administrator',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 33,
                'parent_id' => '5',
                'name' => 'Brand Administrator',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 34,
                'parent_id' => '5',
                'name' => 'Product Manager',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 35,
                'parent_id' => '5',
                'name' => 'Agent',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 36,
                'parent_id' => '5',
                'name' => 'Consultant',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 37,
                'parent_id' => '5',
                'name' => 'Consumer',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 38,
                'parent_id' => '6',
                'name' => 'Accommodation Inventory',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 39,
                'parent_id' => '6',
                'name' => 'Tours / Attractions / Experiences Inventory',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 40,
                'parent_id' => '6',
                'name' => 'Transport Inventory',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 41,
                'parent_id' => '6',
                'name' => 'Packages Inventory',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 42,
                'parent_id' => '6',
                'name' => 'Cruise Inventory',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 43,
                'parent_id' => '6',
                'name' => 'Event / Concert Inventory',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 44,
                'parent_id' => '6',
                'name' => 'Restarurant Inventory',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 45,
                'parent_id' => '6',
                'name' => 'Transpost Passes',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 46,
                'parent_id' => '6',
                'name' => 'Transfers',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 47,
                'parent_id' => '6',
                'name' => 'Car Hire',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 48,
                'parent_id' => '6',
                'name' => 'Suppliers',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 49,
                'parent_id' => '6',
                'name' => 'Special Offers',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 50,
                'parent_id' => '6',
                'name' => 'Free Events / Land Marks',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 51,
                'parent_id' => '7',
                'name' => 'General',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 52,
                'parent_id' => '7',
                'name' => 'Front-End Configuration',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 53,
                'parent_id' => '7',
                'name' => 'Back-End Configuration',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 54,
                'parent_id' => '7',
                'name' => 'Account Configuration',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 55,
                'parent_id' => '7',
                'name' => 'Supplier / API Configuration',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 56,
                'parent_id' => '7',
                'name' => 'Account Configuration',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ];

        foreach ($backendServices as $value) {
            // DB::table('backend_services')->insert($value);
        }
    }
}
