<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DomainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domain = [
            [
                'licensee_id' => '1',
                'domain_name' => 'google.com',
                'cms_name' => 'google.com',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'licensee_id' => '1',
                'domain_name' => 'yahoo.com',
                'cms_name' => 'yahoo.com',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'licensee_id' => '1',
                'domain_name' => 'youtube.com',
                'cms_name' => 'youtube.com',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'licensee_id' => '1',
                'domain_name' => 'zamzer.com',
                'cms_name' => 'zamzer.com',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'licensee_id' => '1',
                'domain_name' => 'hotmail.com',
                'cms_name' => 'hotmail.com',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],

        ];

        foreach ($domain as $value) {
            DB::table('domains')->insert($value);
        }
    }
}
