<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            OleryMappingTableSeeder::class,
            PerSetAccountOptionsSeeder::class,
            LicenseesTableSeeder::class,
            DomainSeeder::class,
            RolesAndPermissionsSeeder::class,
            CmsUserSeeder::class,
            ProductsTableSeeder::class,
            PaymentTermsSeeder::class,
            TemplateTableSeeder::class,
            FrontendConfigurationsSeeder::class,
            DomainProductsTableSeeder::class,
            DomainSocialMediaSeeder::class,
            BackendServiceSeeder::class,
            BackendConfigurationSeeder::class,
            GeoDataTableSeeder::class,
            InventoryMarketPlaceServicesTableSeeder::class,
            InventoryMarketPlaceTableSeeder::class,
            PaymentConfigurationMappingSeeder::class,
            GrossNetConfigurationsSeeder::class,
            MailConfigurationsSeeder::class,
            PayByUserConfigurationSeeder::class,
            SalesRepresentativeSeeder::class,
            AccountNotesSeeder::class,
            ZAirLineSeeder::class,
            ZCitiesSeeder::class,
            ZCitiesLatLangSeeder::class,
            ZCity3IataCodeSeeder::class,
            ZCityImagesSeeder::class,
            ZCountriesSeeder::class,
            ZCurrienciesSeeder::class,
            ZRegionsSeeder::class,
            ZRouteLegsSeeder::class,
            ZRoutePlansSeeder::class,
            ZRoutesSeeder::class,
            ZSuburbsSeeder::class,
            ZTimeZonesSeeder::class,
            ZViatorLocationsSeeder::class,
            CustomerSeeder::class,
            ItineraryTypeSeeder::class
        ]);


    }
}
