<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DomainSocialMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domainSocialMedia = [
            [
                'domain_id' => '1',
                'facebook' => 'www.facebook.com',
                'twitter' => 'www.twitter.com',
                'linkedin' => 'www.linkedin.com',
                'instagram' => 'www.instagram.com',
                'printrest' => 'www.printrest.com',
                'google' => 'www.google.com',
                'youtube' => 'www.youtube.com',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'domain_id' => '2',
                'facebook' => 'www.facebook.com',
                'twitter' => 'www.twitter.com',
                'linkedin' => 'www.linkedin.com',
                'instagram' => 'www.instagram.com',
                'printrest' => 'www.printrest.com',
                'google' => 'www.google.com',
                'youtube' => 'www.youtube.com',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        foreach ($domainSocialMedia as $value) {
            DB::table('domain_social_media')->insert($value);
        }
    }
}
