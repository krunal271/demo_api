<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Tasks;
use Illuminate\Support\Str;
class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notes_array = ['task1','task2','task3','task4','task5','task6','task7','task8','task9','task10','task11','task12','task13','task14'];
        $booking_ids=['1243','32re','rer434er','tr2323'];
//        Tasks::truncate();
        $tasks = [

                'user_id' => rand(1,5),
                'booking_id' => $booking_ids[rand(0,count($booking_ids)-1)],
                'note' => $notes_array[rand(0,count($notes_array)-1)],
                'due_date' => Carbon::now()->toDateString(),
                'snooze_day' => rand(0,5),
                'task_status'=> rand(1,3),
                'status' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];


        for($i=0;$i<20;$i++)
        {
            Tasks::create([
                'user_id' => rand(1,5),
                'customer_name' => Str::random(10),
                'booking_id' => $booking_ids[rand(0,count($booking_ids)-1)],
                'note' => $notes_array[rand(0,count($notes_array)-1)],
                'due_date' => Carbon::now()->toDateString(),
                'task_status'=> rand(1,3),
                'status' => rand(1,2),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
