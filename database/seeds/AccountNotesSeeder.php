<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountNotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accountNotes = [
            [
                'licensee_id' => '1',
                'sales_representative_id' => '1',
                'additional_notes' => 'This is additional notes 1',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'licensee_id' => '2',
                'sales_representative_id' => '1',
                'additional_notes' => 'This is additional notes 2',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ];

        foreach ($accountNotes as $value) {
            DB::table('account_notes')->insert($value);
        }
    }

}
