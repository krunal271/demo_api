<?php

use App\Models\CmsUser;
use App\Models\UserDetail;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CmsUserSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$cmsUser = [
			[
				'domain_id' => 1,
				'username' => 'admin',
				'password' => Hash::make('123456'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'licensee_administrator',
				'details' => [
					'user_type' => '2',
					'first_name' => 'rockey',
					'last_name' => 'hunter',
					'email' => 'rockey.hunter@gmail.com',
					'contact_number' => '9874563210',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'krunal',
				'password' => Hash::make('123456'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'brand_administrator',
				'details' => [
					'user_type' => '2',
					'first_name' => 'krunal',
					'last_name' => 'parikh',
					'email' => 'krunal@loftdigital.com',
					'contact_number' => '9874563211',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'Anthony',
				'password' => Hash::make('123456'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'product_manager',
				'details' => [
					'user_type' => '2',
					'first_name' => 'Anthony',
					'last_name' => '',
					'email' => 'anthony@eroam.com',
					'contact_number' => '9874563212',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'Tom',
				'password' => Hash::make('132456'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'agent',
				'details' => [
					'user_type' => '2',
					'first_name' => 'tom',
					'last_name' => 'cruise',
					'email' => 'tom.cruise@gmail.com',
					'contact_number' => '9874563213',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'Alex',
				'password' => Hash::make('132456'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'consultant',
				'details' => [
					'user_type' => '2',
					'first_name' => 'alex',
					'last_name' => 'carry',
					'email' => 'alex.carry@gmail.com',
					'contact_number' => '9874563214',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'David',
				'password' => Hash::make('132456'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'consumer',
				'details' => [
					'user_type' => '2',
					'first_name' => 'david',
					'last_name' => 'warner',
					'email' => 'david.warner@gmail.com',
					'contact_number' => '9874563215',
				],
			],

			[
				'domain_id' => 1,
				'username' => 'eRoamAPW',
				'password' => Hash::make('C>,fNKkt9zcTNuyLByb2'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'consumer',
				'details' => [
					'user_type' => '2',
					'first_name' => 'david',
					'last_name' => 'warner',
					'email' => 'david.warner@gmail.com',
					'contact_number' => '9874563215',
				],
			],
			//eroam sales team
			[
				'domain_id' => 1,
				'username' => 'rcecconi@sportsnetcorp.com',
				'password' => Hash::make('qpxs?z(yqRAXwyw9u4tC'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'consumer',
				'details' => [
					'user_type' => '2',
					'first_name' => 'Robert',
					'last_name' => 'Cecconi',
					'email' => 'rcecconi@sportsnetcorp.com',
					'contact_number' => '9876543214',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'ignacio@eroam.com',
				'password' => Hash::make('dfsgjp?NfihyfXAR;p48'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'consumer',
				'details' => [
					'user_type' => '2',
					'first_name' => 'Ignacio ',
					'last_name' => '',
					'email' => 'ignacio@eroam.com',
					'contact_number' => '9876543214',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'bob@eroam.com',
				'password' => Hash::make('uEf8dexuFCxhumkR8{Z?'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'consumer',
				'details' => [
					'user_type' => '2',
					'first_name' => 'Bob',
					'last_name' => '',
					'email' => 'bob@eroam.com',
					'contact_number' => '9876543214',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'charlie@eroam.com',
				'password' => Hash::make('{UYa;J9WGWfVzpvy8sUP'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'consumer',
				'details' => [
					'user_type' => '2',
					'first_name' => 'Charlie',
					'last_name' => '',
					'email' => 'charlie@eroam.com',
					'contact_number' => '9876543214',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'nmc010@yahoo.com',
				'password' => Hash::make('rHVtnwi;WDV8wZdTcG8='),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'consumer',
				'details' => [
					'user_type' => '2',
					'first_name' => 'Martin',
					'last_name' => 'Cowley',
					'email' => 'nmc010@yahoo.com',
					'contact_number' => '9876543214',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'paula.halliday@theguardian.com',
				'password' => Hash::make('F)ApAEg8nWQ}3cTvBmDd'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'consumer',
				'details' => [
					'user_type' => '2',
					'first_name' => 'Guardian',
					'last_name' => '',
					'email' => 'paula.halliday@theguardian.com',
					'contact_number' => '9876543214',
				],
			],
			[
				'domain_id' => 1,
				'username' => 'randyg@goodrichtravel.com',
				'password' => Hash::make('FpJ8RBDk8v/{boiJihqG'),
				'status' => '1',
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'role' => 'consumer',
				'details' => [
					'user_type' => '2',
					'first_name' => 'Passport',
					'last_name' => 'Online',
					'email' => 'randyg@goodrichtravel.com',
					'contact_number' => '9876543214',
				],
			],

		];

		foreach ($cmsUser as $value) {
			$role = $value["role"];
			$details = $value["details"];
			unset($value["role"]);
			unset($value["details"]);

			$user = CmsUser::whereUsername($value["username"])->first();
			if ($user) {
				$user->update($value);
			} else {
				$user = CmsUser::create($value);
			}
			$user->assignRole($role);

			if ($user) {
				$details["user_id"] = $user->id;
				$userDetails = UserDetail::whereUserId($user->id)->first();
				if ($userDetails) {
					$userDetails->update($details);
				} else {
					UserDetail::create($details);
				}
			}
		}
	}
}
