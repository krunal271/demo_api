<?php

return [
    'model_not_found' => 'Model not found',
    'url_not_found' => 'URL not found',
    'unauthorized_user' => 'Unauthorized',
    'method_not_found' => 'Method not found',
];
