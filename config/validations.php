<?php

return [
    'addLicenseeValidation' => [
        'rules' => [
            'per_set_account_id' => 'required|numeric',
            'business_name' => 'required',
            'business_email' => 'required|email',
            'business_phone_number' => 'required|numeric',
            'first_name' => 'required',
            'last_name' => 'required',
            'street' => 'required',
            'zip_code' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country_id' => 'required|numeric',
        ],
        'messages' => [
            'per_set_account_id.required' => 'Per set account option required.',
            'per_set_account_id.numeric' => 'Invalid parameter per set account option.',
            'business_name.required' => 'Business name required.',
            'business_email.required' => 'Email required.',
            'business_email.email' => 'Invalid parameter email.',
            'business_phone_number.required' => 'Phone number required.',
            'business_phone_number.numeric' => 'Invalid parameter business phone number.',
            'first_name.required' => 'First name required.',
            'last_name.required' => 'Last name required.',
            'street.required' => 'Street required.',
            'zip_code.required' => 'Zip Code required.',
            'city.required' => 'City required.',
            'state.required' => 'State required.',
            'country_id.required' => 'Country required.',
            'country_id.numeric' => 'Invalid parameter country.'
        ],
    ],
    'editLicenseeValidation' => [
        'rules' => [
            'id' => 'required|numeric',
            'per_set_account_id' => 'required|numeric',
            'business_name' => 'required',
            'business_email' => 'required|email',
            'business_phone_number' => 'required|numeric',
            'first_name' => 'required',
            'last_name' => 'required',
            'street' => 'required',
            'zip_code' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country_id' => 'required|numeric',
        ],
        'messages' => [
            'id.required' => 'Licensee Required.',
            'id.numeric' => 'Invalid parameter licensee.',
            'per_set_account_id.required' => 'Per set account option required.',
            'per_set_account_id.numeric' => 'Invalid parameter per set account option.',
            'business_name.required' => 'Business name required.',
            'business_email.required' => 'Email required.',
            'business_email.email' => 'Invalid parameter email.',
            'business_phone_number.required' => 'Phone number required.',
            'business_phone_number.numeric' => 'Invalid parameter business phone number.',
            'first_name.required' => 'First name required.',
            'last_name.required' => 'Last name required.',
            'street.required' => 'Street required.',
            'zip_code.required' => 'Zip Code required.',
            'city.required' => 'City required.',
            'state.required' => 'State required.',
            'country_id.required' => 'Country required.',
            'country_id.numeric' => 'Invalid parameter country.'
        ],
    ],
    'addFrontendConfiguration' => [
        'rules' => [
            'licensee_id' => 'required|numeric',
            'logo' => 'required',
            'favicon' => 'required',
            'domain_name' => 'required',
            'cms_name' => 'required',
            'template_id' => 'required|numeric',
            'group_one_color' => 'required',
            'group_two_color' => 'required',
            'group_three_color' => 'required',
            'page_title' => 'required',
            'product_id' => 'required'

        ],
        'messages' => [
            'licensee_id.required' => 'Licensee id required.',
            'licensee_id.numeric' => 'Invalid parameter licensee.',
            'logo.required' => 'Logo required.',
            'favicon.required' => 'Favicon required.',
            'domain_name.required' => 'Domain name required.',
            'cms_name.required' => 'CMS name required.',
            'template_id.required' => 'Template Id required.',
            'template_id.numeric' => 'Invalid parameter Template.',
            'group_one_color.required' => 'Group one color(header) required.',
            'group_two_color.required' => 'Group two color(footer) required.',
            'group_three_color.required' => 'Group three color(search panal background) required.',
            'page_title.required' => 'Page title required.',
            'product_id.required' => 'Please select at least one product.'
        ],
    ],
    'editFrontendConfiguration' => [
        'rules' => [
            'licensee_id' => 'required|numeric',
            'logo' => 'required',
            'favicon' => 'required',
            'domain_id' => 'required|numeric',
            'domain_name' => 'required',
            'cms_name' => 'required',
            'template_id' => 'required|numeric',
            'group_one_color' => 'required',
            'group_two_color' => 'required',
            'group_three_color' => 'required',
            'page_title' => 'required',
            'product_id' => 'required'
        ],
        'messages' => [
            'licensee_id.required' => 'Licensee id required.',
            'licensee_id.numeric' => 'Invalid parameter licensee.',
            'logo.required' => 'Logo required.',
            'logo.image' => 'Logo should be as image.',
            'favicon.required' => 'Favicon required.',
            'favicon.image' => 'Favicon should be as image.',
            'domain_id.required' => 'Domain is required.',
            'domain_id.numeric' => 'Invalid parameter domain.',
            'domain_name.required' => 'Domain name required.',
            'cms_name.required' => 'CMS name required.',
            'template_id.required' => 'Template Id required.',
            'template_id.numeric' => 'Invalid parameter Template.',
            'group_one_color.required' => 'Group one color(header) required.',
            'group_two_color.required' => 'Group two color(footer) required.',
            'group_three_color.required' => 'Group three color(search panal background) required.',
            'page_title.required' => 'Page title required.',
            'product_id.required' => 'Please select at least one product.'
        ],
    ],
    'frontendConfigurationDetail' => [
        'rules' => [
            'domain_id' => 'required|numeric'
        ],
        'messages' => [
            'domain_id.required' => 'Domain required.',
            'domain_id.numeric' => 'Invalid parameter domain.'
        ],
    ],
    'licenseeDetailValidation' => [
        'rules' => [
            'licensee_id' => 'required|numeric'
        ],
        'messages' => [
            'licensee_id.required' => 'Licensee required.',
            'licensee_id.numeric' => 'Invalid parameter licensee.'
        ],
    ],
    'backendConfiguration' => [
        'rules' => [
            'domain_id' => 'required|numeric',
            'backend_services_id' => 'required'
        ],
        'messages' => [
            'domain_id.required' => 'Domain required.',
            'domain_id.numeric' => 'Invalid parameter domain.',
            'backend_services_id.required' => 'Please select at least one service.',
        ],
    ],
    'backendConfigurationDetail' => [
        'rules' => [
            'domain_id' => 'required|numeric',
        ],
        'messages' => [
            'domain_id.required' => 'Domain required.',
            'domain_id.numeric' => 'Invalid parameter domain.',
        ],
    ],
    'GeoDataValidation' => [
        'rules' => [
            'domain_id' => 'required|numeric'
        ],
        'messages' => [
            'domain_id.required' => 'Domain required.',
            'domain_id.numeric' => 'Invalid parameter domain.'
        ],
    ],
    'inventoryMarketPlaceValidation' => [
        'rules' => [
            'domain_id' => 'required|numeric',
            'inventory_market_place_service_id|numeric' => 'required'
        ],
        'messages' => [
            'domain_id.required' => 'Domain required.',
            'domain_id.numeric' => 'Invalid parameter domain.',
            'inventory_market_place_service_id.required' => 'Maerket place service required.',
            'inventory_market_place_service_id.numeric' => 'Invalid parameter inventory market place service.'
        ],
    ],
    'addCmsUser' => [
        'rules' => [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'contact_number' => 'required|numeric',
            'domain_id' => 'required|numeric',
            'role' => 'required',
        ],
        'messages' => [
            'first_name.required' => 'First name required.',
            'last_name.required' => 'Last name required.',
            'email.required' => 'Email required.',
            'email.email' => 'Please enter valid email.',
            'contact_number.required' => 'Contact number required.',
            'contact_number.numeric' => 'Please enter valid contact number.',
            'domain_id.required' => 'Please select domain.',
            'domain_id.numeric' => 'Invalid parameter domain.',
            'role.required' => 'Please select role.',
        ],
    ],
    'editCmsUser' => [
        'rules' => [
            'user_id' => 'required|numeric',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'contact_number' => 'required|numeric',
            'domain_id' => 'required|numeric',
            'role' => 'required',
        ],
        'messages' => [
            'user_id.required' => 'User id required.',
            'user_id.numeric' => 'Invalid parameter user.',
            'first_name.required' => 'First name required.',
            'last_name.required' => 'Last name required.',
            'email.required' => 'Email required.',
            'email.email' => 'Please enter valid email.',
            'contact_number.required' => 'Contact number required.',
            'contact_number.numeric' => 'Please enter valid contact number.',
            'domain_id.required' => 'Please select domain.',
            'domain_id.numeric' => 'Invalid parameter domain.',
            'role.required' => 'Please select role.',

        ],
    ],
    'viewAccountConfigurationValidation' => [
        'rules' => [
            'licensee_id' => 'required|numeric'
        ],
        'messages' => [
            'licensee_id.required' => 'Licensee required.',
            'licensee_id.numeric' => 'Invalid parameter licensee.'
        ],
    ],
    'accountConfigurationValidation' => [
        'rules' => [
            'licensee_id' => 'required|numeric',
            'gross_net_configuration.accomodation.rate_type' => 'required',
            'gross_net_configuration.accomodation.commission' => 'required',
            'gross_net_configuration.activity.rate_type' => 'required',
            'gross_net_configuration.activity.commission' => 'required',
            'gross_net_configuration.transport.rate_type' => 'required',
            'gross_net_configuration.transport.commission' => 'required',
            'gross_net_configuration.tour.rate_type' => 'required',
            'gross_net_configuration.tour.commission' => 'required',
            'pay_by_user_configuration.edit_order_id_format' => 'required',
            'pay_by_user_configuration.payment_configuration_language' => 'required',
            'mail_configuration.mail_host' => 'required',
            'mail_configuration.mail_port' => 'required',
            'mail_configuration.mail_username' => 'required',
            'mail_configuration.mail_password' => 'required',
            'mail_configuration.mail_from_address' => 'required',
            'mail_configuration.mail_from_name' => 'required',
            'mail_configuration.mail_encryption' => 'required',

        ],
        'messages' => [
            'licensee_id.required' => 'Licensee required',
            'licensee_id.numeric' => 'Invalid parameter licensee.',
            'gross_net_configuration.accomodation.rate_type.required' => 'Accomodation Rate Type required',
            'gross_net_configuration.accomodation.commission.required' => 'Accomodation Commission required',
            'gross_net_configuration.activity.rate_type.required' => 'Activity Rate Type required',
            'gross_net_configuration.activity.commission.required' => 'Activity Commission required',
            'gross_net_configuration.transport.rate_type.required' => 'Transpost Rate Type required',
            'gross_net_configuration.transport.commission.required' => 'Transpost Commission required',
            'gross_net_configuration.tour.rate_type.required' => 'Tour Rate Type required',
            'gross_net_configuration.tour.commission.required' => 'Tour Commission required',
            'pay_by_user_configuration.edit_order_id_format.required' => 'Edit order id format required',
            'pay_by_user_configuration.payment_configuration_language.required' => 'Payment Configuration language required',
            'mail_configuration.mail_host.required' => 'Mail host required',
            'mail_configuration.mail_port.required' => 'Mail port required',
            'mail_configuration.mail_username.required' => 'Mail Username required',
            'mail_configuration.mail_password.required' => 'Mail Password required',
            'mail_configuration.mail_from_address.required' => 'Mail From address required',
            'mail_configuration.mail_from_name.required' => 'Mail From Name required',
            'mail_configuration.mail_encryption.required' => 'Mail Encryption required',
        ],
    ],
    'accountNotesValidation' => [
        'rules' => [
            'licensee_id' => 'required|numeric',
            'sales_representative_id' => 'required|numeric',
            'additional_notes' => 'required'
        ],
        'messages' => [
            'licensee_id.required' => 'Licensee required.',
            'licensee_id.numeric' => 'Invalid parameter licensee.',
            'sales_representative_id.required' => 'Sales Representative required.',
            'sales_representative_id.numeric' => 'Invalid parameter Sales Representative.',
            'additional_notes.required' => 'Additional Notes required.'
        ],
    ],
    'oleryMapping' => [
        'rules' => [
            'expedia_id' => 'required|numeric'
        ],
        'messages' => [
            'expedia_id.required' => 'Expedia required.',
            'expedia_id.numeric' => 'Invalid parameter Expedia.'
        ],
    ],
    'cmsUserList' => [
        'rules' => [
            'domain_id' => 'required|numeric'
        ],
        'messages' => [
            'domain_id.required' => 'Please select domain.',
            'domain_id.numeric' => 'Invalid parameter domain.'
        ],
    ],
    'AddCustomerValidation' => [
        'rules' => [
            'customer_id' => 'required',
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'country_code' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'contact_method' => 'required',
            'domain' => 'required',
            'frequently_used_product' => 'required',
            'accommodation_type' => 'required',
            'room_type' => 'required',
            'transport_type' => 'required',
            'cabin_class_type' => 'required',
            'seat_type' => 'required',
            'meal_type' => 'required',
            'interests' => 'required',
            'conversation_notes' => 'required',
        ],
        'messages' => [
            'customer_id.required'=>'Customer id required.',
            'title.required'=>'Title required.',
            'first_name.required'=>'First name required.',
            'last_name.required'=>'Last name required.',
            'gender.required'=>'Gender required.',
            'country_code.required'=>'Country code required.',
            'phone.required'=>'Phone number required.',
            'email.required'=>'Email required.',
            'contact_method.required'=>'Contact method required.',
            'domain.required'=>'Domain required.',
            'frequently_used_product.required'=>'Frequently used product required.',
            'accommodation_type.required'=>'Accommodation type required.',
            'room_type.required'=>'Room type required.',
            'transport_type.required'=>'Transport type required.',
            'cabin_class_type.required'=>'Cabin class type required.',
            'seat_type.required'=>'Seat type required.',
            'meal_type.required'=>'Meal type required.',
            'interests.required'=>'Interests required.',
            'conversation_notes.required'=>'Conversation note required.',
        ],
    ],
    'EditCustomerValidation' => [
        'rules' => [
            'id'=>'required',
            'customer_id' => 'required',
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'country_code' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'contact_method' => 'required',
            'domain' => 'required',
            'frequently_used_product' => 'required',
            'accommodation_type' => 'required',
            'room_type' => 'required',
            'transport_type' => 'required',
            'cabin_class_type' => 'required',
            'seat_type' => 'required',
            'meal_type' => 'required',
            'interests' => 'required',
            'conversation_notes' => 'required',
        ],
        'messages' => [
            'id.required'=>'Id required.',
            'customer_id.required'=>'Customer id required.',
            'title.required'=>'Title required.',
            'first_name.required'=>'First name required.',
            'last_name.required'=>'Last name required.',
            'gender.required'=>'Gender required.',
            'country_code.required'=>'Country code required.',
            'phone.required'=>'Phone number required.',
            'email.required'=>'Email required.',
            'contact_method.required'=>'Contact method required.',
            'domain.required'=>'Domain required.',
            'frequently_used_product.required'=>'Frequently used product required.',
            'accommodation_type.required'=>'Accommodation type required.',
            'room_type.required'=>'Room type required.',
            'transport_type.required'=>'Transport type required.',
            'cabin_class_type.required'=>'Cabin class type required.',
            'seat_type.required'=>'Seat type required.',
            'meal_type.required'=>'Meal type required.',
            'interests.required'=>'Interests required.',
            'conversation_notes.required'=>'Conversation note required.',
        ],
    ],

];
