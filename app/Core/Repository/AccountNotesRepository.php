<?php

namespace App\Core\Repository;

use App\Models\AccountNotes;
use App\Models\SalesRepresentative;
use App\Models\Status;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AccountNotesRepository
{

    /**
     * Add and returns account notes information
     *
     * @param $request
     * @return array
     */
    public function addAccountNotes($request)
    {
        return $this->saveAccountNotes($request);
    }

    /**
     * Edit and returns the account notes information
     *
     * @param $request
     * @return array
     */
    public function editAccountNotes($request)
    {
        return $this->saveAccountNotes($request);
    }

    /**
     * Common function for save Account notes information
     *
     * @param $request
     * @return array
     */
    private function saveAccountNotes($request)
    {
        $requestParam = $request->all();
        DB::beginTransaction();
        try {
            $accountNotes = AccountNotes::whereLicenseeId($requestParam['licensee_id'])->whereStatus(Status::$ACTIVE)->first();
            if (!$accountNotes) {
                $accountNotes = new AccountNotes;
            }
            $accountNotes->licensee_id = $requestParam['licensee_id'];
            $accountNotes->sales_representative_id = $requestParam['sales_representative_id'];
            $accountNotes->additional_notes = $requestParam['additional_notes'];
            $accountNotes->status = Status::$ACTIVE;
            $accountNotes->save();
        } catch (QueryException $e) {
            DB::rollBack();
            Log::error('AccountNotesRepository Exception: ' . $e->getMessage());
            return ['code' => 400, 'message' => 'Something went wrong'];
        }
        DB::commit();
        $response = AccountNotes::whereId($accountNotes->id)->select('id', 'licensee_id', 'sales_representative_id',
            'additional_notes', 'status')->first();
        if ($response) {
            return ['code' => 200, 'data' => $response];
        } else {
            return ['code' => 400];
        }
    }


    /**
     * Returns the lis of sales representative
     *
     * @return array
     */
    public function salesRepresentativeList()
    {
        $response = SalesRepresentative::whereStatus(Status::$ACTIVE)->select('id as value', 'name', 'status')->get();
        return ['code' => 200, 'data' => $response];
    }
}