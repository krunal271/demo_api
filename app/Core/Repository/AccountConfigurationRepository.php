<?php

namespace App\Core\Repository;

use App\Models\GrossNetConfigurations;
use App\Models\Licensees;
use App\Models\MailConfigurations;
use App\Models\PayByUserConfiguration;
use App\Transformers\AccountConfigurationTransformer;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class AccountConfigurationRepository
{
    protected $fractal;

    public function __construct()
    {
        $this->fractal = new Manager;
    }
    /**
     * Returns account configuration details as per licensee_id
     *
     * @param $request
     * @return array
     */
    public function viewAccountConfiguration($request)
    {
        $requestParam = $request->all();
        $licensee = Licensees::where('id', $requestParam['licensee_id'])->first();
        if(!$licensee)
        {
            return ['code' => 204, 'message' => 'Licensee Not Found'];
        }
        $responseData = new Item($requestParam, new AccountConfigurationTransformer);
        $response = $this->fractal->createData($responseData)->toArray();
        if ($response) {
            return ['code' => 200, 'data' => $response['data']];
        } else {
            return ['code' => 400];
        }
    }

    /**
     * add Account Configuration and return the newly generated response
     *
     * @param $request
     * @return array
     */
    public function addAccountConfiguration($request)
    {
        return $this->saveAccountConfiguration($request);
    }

    /**
     * edit Account Configuration and return the newly generated response
     *
     * @param $request
     * @return array
     */
    public function editAccountConfiguration($request)
    {
        return $this->saveAccountConfiguration($request);
    }

    /**
     * add-edit and returns Account Configuration and return the newly generated response
     *
     * @param $request
     * @return array
     */
    private function saveAccountConfiguration($request)
    {
        $requestParam = $request->all();
        $licensee = Licensees::where('id', $requestParam['licensee_id'])->first();
        if(!$licensee)
        {
            return ['code' => 204, 'message' => 'Licensee Not Found'];
        }
        DB::beginTransaction();
        $grossNetConfiguration = [];
        $paymentTermIds = $requestParam['payment_term_ids'];
        $grossConfigurationDetails = $requestParam['gross_net_configuration'];
        try {
            if (isset($requestParam['licensee_id']) && $requestParam['licensee_id'] !== null) {
                //sync payment terms
                $licensee = Licensees::where('id', $requestParam['licensee_id'])->first();
                if (!$licensee) {
                    $licensee = new Licensees;
                }
                $licensee->paymentConfiguration()->sync($paymentTermIds);

                //Gross Net Configuration
                foreach ($grossConfigurationDetails as $key => $value) {
                    $grossConfiguration = GrossNetConfigurations::where('name',
                        $key)->whereLicenseeId($requestParam['licensee_id'])->first();
                    if (!$grossConfiguration) {
                        $grossConfiguration = new GrossNetConfigurations;
                    }
                    $grossConfiguration->licensee_id = $requestParam['licensee_id'];
                    $grossConfiguration->name = $key;
                    $grossConfiguration->rate_type = isset($value['rate_type']) ? $value['rate_type'] : '0';
                    $grossConfiguration->commission = isset($value['commission']) ? $value['commission'] : '';
                    $grossConfiguration->agent_enterprice = isset($value['agent_enterprice']) ? $value['agent_enterprice'] : '';
                    $grossConfiguration->agent_wholesale = isset($value['agent_wholesale']) ? $value['agent_wholesale'] : '';
                    $grossConfiguration->save();
                    $grossNetConfiguration[] = (string)$grossConfiguration->name;
                }

                //In case of edit (remove old payment terms from domain)
                if (count($grossNetConfiguration) > 0) {
                    GrossNetConfigurations::whereNotIn("name", $grossNetConfiguration)
                        ->whereLicenseeId($requestParam['licensee_id'])->delete();
                }

                //Pay by user configuration
                $payByUserConfigDetail = $requestParam['pay_by_user_configuration'];
                $payByUserConfiguration = PayByUserConfiguration::whereLicenseeId($requestParam['licensee_id'])->first();
                if (!$payByUserConfiguration) {
                    $payByUserConfiguration = new PayByUserConfiguration;
                }
                $payByUserConfiguration->licensee_id = $requestParam['licensee_id'];
                $payByUserConfiguration->amount_per_user = isset($payByUserConfigDetail['amount_per_user']) ? $payByUserConfigDetail['amount_per_user'] : 0;
                $payByUserConfiguration->payment_frequency = isset($payByUserConfigDetail['payment_frequency']) ? $payByUserConfigDetail['payment_frequency'] : '';
                $payByUserConfiguration->payment_provider = isset($payByUserConfigDetail['payment_provider']) ? $payByUserConfigDetail['payment_provider'] : '';
                $payByUserConfiguration->client_custom_payment = isset($payByUserConfigDetail['client_custom_payment']) ? $payByUserConfigDetail['client_custom_payment'] : '';
                $payByUserConfiguration->manual_payment = isset($payByUserConfigDetail['manual_payment']) ? $payByUserConfigDetail['manual_payment'] : '';
                $payByUserConfiguration->taxes = isset($payByUserConfigDetail['taxes']) ? $payByUserConfigDetail['taxes'] : 0;
                $payByUserConfiguration->edit_order_id_format = isset($payByUserConfigDetail['edit_order_id_format']) ? $payByUserConfigDetail['edit_order_id_format'] : '';
                $payByUserConfiguration->consumer_accounts = isset($payByUserConfigDetail['consumer_accounts']) ? $payByUserConfigDetail['consumer_accounts'] : '';
                $payByUserConfiguration->payment_configuration_language = isset($payByUserConfigDetail['payment_configuration_language']) ? $payByUserConfigDetail['payment_configuration_language'] : '';
                $payByUserConfiguration->save();

                //Mail Configuration
                $mailConfigDetails = $requestParam['mail_configuration'];
                $mailConfiguration = MailConfigurations::whereLicenseeId($requestParam['licensee_id'])->first();
                if (!$mailConfiguration) {
                    $mailConfiguration = new MailConfigurations;
                }
                $mailConfiguration->licensee_id = isset($requestParam['licensee_id']) ? $requestParam['licensee_id'] : '';
                $mailConfiguration->mail_host = isset($mailConfigDetails['mail_host']) ? $mailConfigDetails['mail_host'] : '';
                $mailConfiguration->mail_port = isset($mailConfigDetails['mail_port']) ? $mailConfigDetails['mail_port'] : '';
                $mailConfiguration->mail_username = isset($mailConfigDetails['mail_username']) ? $mailConfigDetails['mail_username'] : '';
                $mailConfiguration->mail_password = isset($mailConfigDetails['mail_password']) ? $mailConfigDetails['mail_password'] : '';
                $mailConfiguration->mail_from_address = isset($mailConfigDetails['mail_from_address']) ? $mailConfigDetails['mail_from_address'] : '';
                $mailConfiguration->mail_from_name = isset($mailConfigDetails['mail_from_name']) ? $mailConfigDetails['mail_from_name'] : '';
                $mailConfiguration->mail_encryption = isset($mailConfigDetails['mail_encryption']) ? $mailConfigDetails['mail_encryption'] : '';
                $mailConfiguration->save();
            }
        } catch (QueryException $e) {
            DB::rollBack();
            Log::error('AccountConfiguration Exception:' . $e->getMessage());
            return ['code' => 400, 'message' => 'Something went wrong'];
        }
        DB::commit();
        $responseData = new Item($requestParam, new AccountConfigurationTransformer);
        $response = $this->fractal->createData($responseData)->toArray();
        if ($response) {
            return ['code' => 200, 'data' => $response['data']];
        } else {
            return ['code' => 400];
        }
    }
}