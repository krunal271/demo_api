<?php

namespace App\Core\Repository;

use App\Models\CmsUser;
use App\Models\Domain;
use App\Models\UserDetail;
use App\Transformers\CmsUserTransformer;
use App\Transformers\RoleAndPermissionTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use League\Fractal\Manager;

class UserManagementRepository
{

    protected $fractal;

    public function __construct()
    {
        $this->fractal = new Manager;
    }

    /**
     * Return List of roles
     *
     * @return array
     */
    public function getAllRoles()
    {
        $roles = Role::select('id as value', 'name')->get();
        return ['code' => 200, 'data' => $roles];
    }

    /**
     * add cms user
     *
     * @param Request $request
     * @return array
     */
    public function addCmsUser(Request $request)
    {
        return $this->saveCmsUser($request);
    }

    /**
     * edit cms user
     *
     * @param Request $request
     * @return array
     */
    public function editCmsUser(Request $request)
    {
        return $this->saveCmsUser($request);
    }

    /**
     * save cms user
     *
     * @param Request $request
     * @return array
     */
    private function saveCmsUser(Request $request)
    {
        $cmsData = $request->all();
        $domain = Domain::whereId($cmsData["domain_id"])->first();
        $isNewUser = false;
        if (!$domain) {
            return ['code' => 204, 'message' => 'Domain not found'];
        }
        if (isset($cmsData["user_id"]) && $cmsData["user_id"] != null) {
            $cmsUser = CmsUser::whereId($cmsData["user_id"])->first();
            if (!$cmsUser) {
                return ['code' => 204, 'message' => 'User not found'];
            }
            //check email in case of update email
            $userExists = $this->checkCmsUserByDomain($cmsData["domain_id"], $cmsData["email"], $cmsData["user_id"]);
        } else {
            //check user exists in domain
            $userExists = $this->checkCmsUserByDomain($cmsData["domain_id"], $cmsData["email"]);
            $isNewUser = true;
        }
        if ($userExists == true) {
            return ['code' => 208, 'message' => 'User already exists'];
        }

        DB::beginTransaction();
        try {
            if ($isNewUser == true) {
                $cmsUser = new CmsUser;
                $cmsUser->domain_id = $cmsData["domain_id"];
                $cmsUser->password = Hash::make("user@123456");
                $cmsUser->created_by = 0;
            }
            $cmsUser->updated_by = 0;
            $cmsUser->username = $cmsData["email"];
            $cmsUser->save();

            //add-edit cms user role
            $cmsUser->syncRoles($cmsData["role"]);

            //add-edit user details
            $userDetails = UserDetail::whereUserId($cmsUser->id)->first();
            if (!$userDetails) {
                $userDetails = new UserDetail;
                $userDetails->user_id = $cmsUser->id;
            }
            $userDetails->user_type = UserDetail::$CMSADMIN;
            $userDetails->first_name = $cmsData["first_name"];
            $userDetails->last_name = $cmsData["last_name"];
            $userDetails->email = $cmsData["email"];
            $userDetails->contact_number = $cmsData["contact_number"];
            $userDetails->save();
        } catch (QueryException $ex) {
            DB::rollBack();
            Log::error("UserManagement Exception" . $ex->getMessage());
            return ['code' => 400, 'message' => 'Something went wrong'];
        }
        DB::commit();
        if ($cmsUser) {
            $userDetails->role = $cmsUser->role_name;
            return ['code' => 200, 'data' => $userDetails];
        } else {
            return ['code' => 400];
        }
    }

    /**
     * check email exist
     *
     * @param $domainId
     * @param $username
     * @param null $userId
     * @return bool
     */
    public function checkCmsUserByDomain($domainId, $username, $userId = null)
    {
        $userExists = false;
        if ($userId != null) {
            $cmsUser = CmsUser::where("id", "!=", $userId)->whereDomainId($domainId)->whereUsername($username)->first();
        } else {
            $cmsUser = CmsUser::whereDomainId($domainId)->whereUsername($username)->first();
        }
        if ($cmsUser) {
            $userExists = true;
        }
        return $userExists;
    }

    /**
     * Cms user list
     *
     * @param Request $request
     * @return array
     */
    public function cmsUserList(Request $request)
    {
        $requestParams = $request->all();
        $domainId = $requestParams["domain_id"];

        $cmsUsers = CmsUser::whereDomainId($domainId)->with('userDetails')->get();
        $responseData = new Collection($cmsUsers, new CmsUserTransformer);
        $response = $this->fractal->createData($responseData)->toArray();
        return ['code' => 200, 'data' => $response['data']];
    }

    /**
     * Role-wise permissions list
     *
     * @param Request $request
     * @return array
     */
    public function rolesPermissionsList(Request $request)
    {
        $roleAndPermission = Role::with('permissions')->get();

        $responseData = new Collection($roleAndPermission, new RoleAndPermissionTransformer);
        $response = $this->fractal->createData($responseData)->toArray();
        return ['code' => 200, 'data' => $response['data']];
    }

    /**
     * Permissions list
     *
     * @param Request $request
     * @return array
     */
    public function permissionsList(Request $request)
    {
        $permissions = Permission::select('id as value', 'name')->get();
        return ['code' => 200, 'data' => $permissions];

    }

    /**
     * User Detail
     *
     * @param Request $request
     * @return array
     */
    public function cmsUserDetail(Request $request){
        $requestParam = $request->all();
        $cmsUser = CmsUser::whereId($requestParam['id'])->first();
        $responseData = new Item($cmsUser, new CmsUserTransformer);
        $response = $this->fractal->createData($responseData)->toArray();
        return ['code' => 200, 'data' => $response['data']];
    }
}