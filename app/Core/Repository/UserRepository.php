<?php

namespace App\Core\Repository;
use App\User;
use Illuminate\Support\Facades\Hash;
class UserRepository{

	public function login($user_details){
		$user = User::where('username', $user_details['username'])->first();
		if(Hash::check($user_details['password'], $user->password)){
			$tokenobj = $user->createToken('eroam_token');
			return ['code'=>200,'token'=>$tokenobj->accessToken,'message'=>"User Loggedin"];
		}
		else{
			return ['code'=>'401','message'=>"Invalid Username or Password"];
		}
	}
}