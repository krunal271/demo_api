<?php

namespace App\Core\Repository;

use App\Models\BackendConfiguration;
use App\Models\BackendService;
use App\Models\Domain;
use App\Transformers\BackEndConfigurationTransformer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class BackendConfigurationRepository
{
    protected $fractal;

    public function __construct()
    {
        $this->fractal = new Manager;
    }

    /**
     * backend services
     *
     * @return array
     */
    public function backendServices()
    {
        $response = BackendService::select("id", "name", "parent_id")->whereParentId('0')->with('children')->get();
        return ['code' => 200, 'data' => $response];
    }

    /**
     * add and returns backend configuration settings
     *
     * @param Request $request
     * @return array
     */
    public function addBackendConfiguration(Request $request)
    {
        return $this->saveBackendConfiguration($request);
    }

    /**
     * edit and returns backend configuration settings
     *
     * @param Request $request
     * @return array
     */
    public function editBackendConfiguration(Request $request)
    {
        return $this->saveBackendConfiguration($request);
    }

    /**
     * common add-edit backend configuration function
     *
     * @param Request $request
     * @return array
     */
    private function saveBackendConfiguration(Request $request)
    {
        $backendServicesIds = [];
        $serviceIds = explode(',', $request['backend_services_id']);

        $domain = Domain::whereId($request['domain_id'])->first();
        if (!$domain) {
            return ['code' => 204, 'message' => 'Domain not found.'];
        }

        DB::beginTransaction();
        try {
            //add products
            foreach ($serviceIds as $serviceId) {
                $backendConfiguration = BackendConfiguration::whereDomainId($request['domain_id'])
                    ->whereBackendServicesId($serviceId)->first();
                if (!$backendConfiguration) {
                    $backendConfiguration = new BackendConfiguration;
                    $backendConfiguration->domain_id = $request['domain_id'];
                    $backendConfiguration->backend_services_id = $serviceId;
                    $backendConfiguration->save();
                }
                $backendServicesIds[] = (int)$backendConfiguration->backend_services_id;
            }
            //In case of edit (remove old service from domain)
            if (count($backendServicesIds) > 0) {
                BackendConfiguration::whereNotIn("backend_services_id", $backendServicesIds)
                    ->whereDomainId($request['domain_id'])->delete();
            }
        } catch (QueryException $e) {
            DB::rollBack();
            Log::error('BackendConfiguration Exception: ' . $e->getMessage());
        }
        DB::commit();

        $domain = Domain::select("id", "domain_name")->whereId($request['domain_id'])->first();
        if ($domain) {
            $response["domain_id"] = $domain->id;
            $response["domain_name"] = $domain->domain_name;

            $backendConfiguration = [];
            foreach ($domain->backendConfiguration()->get() as $key => $backConfig) {
                $backendConfiguration[$key]["backend_services_id"] = $backConfig->backend_services_id;
                $backendConfiguration[$key]["name"] = $backConfig->name;
            }
            $response["backendConfiguration"] = $backendConfiguration;

            return ['code' => 200, 'data' => $response];
        } else {
            return ['code' => 400];
        }
    }

    /**
     * Returns the details of back-end configuration
     *
     * @param $request
     * @return array
     */
    public function backendConfigurationDetail($request)
    {
        $requestParam = $request->all();
        $response = Domain::whereId($requestParam['domain_id'])->first();
        if (!$response) {
            return ['code' => 204, 'message' => 'Domain not found'];
        }
        $responseData = new Item($response, new BackEndConfigurationTransformer);
        $response = $this->fractal->createData($responseData)->toArray();
        if ($response) {
            return ['code' => 200, 'data' => $response['data']];
        } else {
            return ['code' => 400];
        }
    }

}