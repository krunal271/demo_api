<?php 

namespace App\Core\Repository;

use App\Models\Tasks;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\Transformers\TaskTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class TaskRepository{

    protected $fractal;
    
    public function __construct()
    {
        $this->fractal = new Manager;
    }

    public function getTaskList($request)
    {
        $taskStatus = $request['task_status'];

        $taskList = Tasks::active()
                ->when($taskStatus == Tasks::ACTION,function($q){
                    $q->action();
                })->when($taskStatus == Tasks::SNOOZE,function($q){
                    $q->snooze();
                })->when($taskStatus == Tasks::COMPLETE,function($q){
                    $q->complete();
                })->whereuserId($request['userId'])->orderBy('id','DESC')->paginate($request['perPage']);

//        $countByStatus = Tasks::active()->groupBy('task_status')->select('task_status', DB::raw('count(task_status) as total'))
//            ->whereuserId($request['userId'])->get()
        $countByStatus['actionTotal'] = Tasks::active()->action()->count();
        $countByStatus['snoozeTotal'] = Tasks::active()->snooze()->count();
        $countByStatus['completeTotal'] = Tasks::active()->complete()->count();
        //we need to convert it into fractal transformer
        /*$responseData = new Collection($TaskList, new LicenseeTransformer);
        $response = $this->fractal->createData($responseData)->toArray();*/
        return ['code' => 200, 'data' => [
                                'taskList'=>$taskList,
                                'taskStatusTotal'=>$countByStatus,
                                ]
                ];
    }

	public function addTaskData($request)
    {
        return $this->saveTaskData($request);
    }

    public function saveTaskData($request)
    {
        $message= "Task Added Successfully";
         if (isset($request['id']) && $request['id']!= '') {
             $message= "Task Edited Successfully";
             $tasks = Tasks::whereId($request['id'])->first();
        } else {
            $tasks = new Tasks;
        }
        
        try {

            //Store Data in tasks table
//            dd($request)
//            changeDateformat
//            $taskStatus = Carbon::parse($request['due_date'])->format('Y-m-d') == Carbon::now()->toDateString() ? Tasks::ACTION : Tasks::SNOOZE;
            $tasks->user_id = $request['userId'];
            $tasks->customer_name = $request['customer_name'];
            $tasks->booking_id = $request['booking_id'];
            $tasks->note = $request['note'];
            $tasks->due_date = $request['due_date'];
            $tasks->task_status = Tasks::TASK_STATUS_INCOMPLETE;
            $tasks->save();
            DB::commit();
            
            $taskDetail = Tasks::whereId($tasks->id)->first();
            /*
            we need to convert it into fractal transformer
            $responseData = new Item($taskDetail, new TaskTransformer());
            $tasks = $this->fractal->createData($responseData)->toArray();*/
            
        } catch (QueryException $e) {
            DB::rollBack();
            return ['code' => 400, 'message' => 'Something went wrong'];
        }
        if ($tasks) {
            return ['code' => 200, 'data' => $taskDetail,'message'=>$message];
        } else {
            return ['code' => 400];
        }
    }
    public function changeDateformat($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }
    public function editTaskStatus($request)
    {

    	$requestParam = $request;
        try {
            $Tasks = Tasks::where('id',$requestParam['id'])->first();
            if (!$Tasks) {
                $Tasks = new Tasks;
            }
            $Tasks->status = $requestParam['status'];
            $Tasks->save();

            return ['code' => 200, 'data' => $Tasks];
        } catch (QueryException $e) {
            DB::rollBack();
            Log::error('TaskRepository Exception: ' . $e->getMessage());
            return ['code' => 400, 'message' => 'Something went wrong'];
        }
        DB::commit();
//        $response = Tasks::where('id',$requestParam['id'])->first();
//        if ($response) {
//            return ['code' => 200, 'data' => $response];
//        } else {
//            dd('dssdd');
//            return ['code' => 400];
//        }
    }

    public function addToComplete($request){
        $tasks = Tasks::whereUserId($request['userId'])->whereIn('id',$request['ids'])->update([
           'task_status'=> Tasks::TASK_STATUS_COMPLETE
        ]);
        return ['code' => 200, 'data' => $tasks];
    }


    public function deleteTask($request){
        if(!$task = Tasks::whereUserId($request['userId'])->whereId($request['id'])->first())
            return ['code' => 400];

        $task->delete();
        return ['code' => 200, 'data' => [] ,'msg'=>"Tasks Added to Complete"];
    }

    public function addtoSnooze($request){

        $dueDate = $this->changeDateformat($request['due_date']);
        $tasks = Tasks::whereUserId($request['userId'])->whereId($request['id'])->update([
            'due_date' => $dueDate
        ]);
        return ['code' => 200, 'data' => $tasks];
    }

}