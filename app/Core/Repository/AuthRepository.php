<?php

namespace App\Core\Repository;

use App\Http\Controllers\Controller;
use App\Models\CmsUser;
use App\Transformers\AuthTransformer;
use App\Transformers\GeoDataTransformer;
use Illuminate\Support\Facades\Hash;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class AuthRepository
{
    protected $fractal;

    public function __construct()
    {
        $this->fractal = new Manager;
    }

    /**
     * Authentication for user
     *
     * @param $request
     * @return array
     */
    public function loginCmsUser($request)
    {
        $requestParams = $request->all();
        $cmsUser = CmsUser::whereUsername($requestParams['username'])->first();
        if (!$cmsUser) {
            return ['code' => 400, 'message' => 'User not found.'];
        }

        if ($cmsUser && Hash::check($requestParams['password'], $cmsUser->password) == false) {
            return ['code' => 400, 'message' => 'Password does not match.'];
        }
        $token = Controller::generateAccessToken($cmsUser);
        if ($token) {
            $cmsUser->token = $token;
            $responseData = new Item($cmsUser, new AuthTransformer);
            $response = $this->fractal->createData($responseData)->toArray();
            if ($response) {
                return ['code' => 200, 'data' => $response['data']];
            } else {
                return ['code' => 400, 'message' => 'Something went wrong.'];
            }
        } else {
            return ['code' => 400, 'message' => 'Something went wrong.'];
        }
    }
}