<?php

namespace App\Core\Repository;

use App\Core\Repository\HotelsRepository;
use App\Core\Repository\VoyegerHotelRepository;
use App\Core\Repository\ActivityRepository;
use App\Core\Repository\TransportRepository;

class SearchRepository{
	protected $hotelsRepository;
	protected $activityRepository;
	protected $transportRepository;
	function __construct(){

	}
	public function searchHotels($origin_city,$checkin_date,$search_preference,$search_text,$default_nights){
		$hotelsRepository = new HotelsRepository;
		$response = $hotelsRepository->getDefaultHotels($origin_city,$checkin_date,$search_preference,$search_text, $default_nights);
		return $response;
	}

	public function prefetchTransport($origin_city,$destination_city,$checkin_date,$selected_route){
		$transportRepository = new TransportRepository;
		if(isset($selected_route['routes']) && !empty($selected_route['routes'])){
			$destination_city = $selected_route['routes'][0][1]['city_id'];
		}

		$response = $transportRepository->getTransportDefault($origin_city,$destination_city,$checkin_date);

		return $response;
	}
	public function searchTransport($origin_city,$destination_city,$checkin_date,$travel_date,$transport_type,$pax_info){
		$transportRepository = new TransportRepository;
		$response = $transportRepository->getTransportDefault($origin_city,$destination_city,$checkin_date,$travel_date,$transport_type,$pax_info);
		return $response;
	}

	public function searchActivities($origin_city,$checkin_date,$search_preference,$pax_info)
	{
		$activityRepository = new ActivityRepository;
		$response = $activityRepository->getDefaulActivity($origin_city,$checkin_date,$search_preference,$pax_info);
		return $response;
	}

	public function searchVoyegerHotels($origin_city,$checkin_date,$search_preference,$search_text,$default_nights)
	{	
		$voyegerHotelsRepository = new VoyegerHotelRepository;
		$response = $voyegerHotelsRepository->getDefaultHotels($origin_city,$checkin_date,$search_preference,$search_text, $default_nights);
		return $response;
	}
}