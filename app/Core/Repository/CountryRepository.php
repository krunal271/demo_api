<?php

namespace App\Core\Repository;

use App\Models\Region;
use App\Models\Country;

class CountryRepository{

	function __construct(){

	}
    public static function findRegionId($region_name){
        $region = Region::where('name', 'like', '%'.$region_name.'%')->orWhere('name', 'like', '%'.$region_name.'/%')->orWhere('name', 'like', '%'.$region_name.' /%')->orWhere('name', 'like', '%/'.$region_name.'%')->orWhere('name', 'like', '%/ '.$region_name.'%')->first();

        return $region['id'];
    }
}