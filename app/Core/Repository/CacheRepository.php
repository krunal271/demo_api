<?php

namespace App\Core\Repository;

use App\Events\SaveToRedisCache;
use App\Models\City;
use App\Models\CityIata;
use App\Models\Country;
use App\Models\PerSetAccountOptions;
use App\Models\RoutePlan;
use App\Models\Airline;
use App\Models\Latlong;
use App\Models\Templates;
use App\Models\Timezone;
use App\Models\ViatorLocation;
use App\Models\CategoryDef;
use Log;
use Illuminate\Support\Facades\Cache as Cache;
use App\Transformers\CityTransformer;
use League\Fractal;
use League\Fractal\Resource\Item;

class CacheRepository
{
    protected $fractal;
    protected $manager;
    function __construct(){
        $this->fractal = new Fractal\Manager;
    }
    public function getAllCities()
    {
        if (!Cache::has('all_cities')) {
            // Added whereNotNull condition because currently we don't have some cities country_id and default_nights and it causes error on front for map
            $cities['level_1'] = City::where('level', 1)->whereNotNull('default_nights')->whereNotNull('country_id')->select('id as value', 'name',
                'country_id')->get()->map(function ($item) {
                return ['code'=>$item['value'],'name'=>$item['label']];
            });
            $cities['level_2'] = City::whereIn('level', [2, 3, 4])->whereNotNull('default_nights')->whereNotNull('country_id')->select('id as value', 'name',
                'country_id')->get()->map(function ($item) {
                return ['code'=>$item['value'],'name'=>$item['label']];
            });
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('all_cities', $cities, $expiresAt);
        } else {
            $cities = Cache::get('all_cities');
        }
        return $cities;
    }

    public function getCityById($city_id)
    {
        if (!Cache::has('cities_by_key_id')) {
            $city = City::with(['country', 'latlong'])->get()->keyBy('id');
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('cities_by_key_id', $city, $expiresAt);
        } else {
            $city = Cache::get('cities_by_key_id');
        }
        return $city[$city_id];
    }
    public function getRouteCityById($city_id){
        if (!Cache::has('cities_by_key_id')) {
            $city = City::with(['country', 'latlong'])->get()->keyBy('id');
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('cities_by_key_id', $city, $expiresAt);
        } else {
            $city = Cache::get('cities_by_key_id');
        }
        $city = new Item($city[$city_id], new CityTransformer());
        $city = $this->fractal->createData($city)->toArray();
        return $city['data'];
    }

    public function getMapRoute($origin_city, $source_city)
    {
        $city_with_lat_long = $this->getCitiesWithLatLongByKeyId();
        $response['type'] = 'auto';
        $response['routes'] = [];
        $response['routes']['cities'] = [];
        $response['routes']['cities'][0] = $city_with_lat_long[$origin_city];
        $response['routes']['cities'][1] = $city_with_lat_long[$source_city];
        return $response;
    }

    public function getCitiesWithLatLong()
    {
        if (!Cache::has('city_with_langlot')) {
            $city_ids_with_latlong = LatLong::pluck('city_id');
            $cities = City::select('zcities.id', 'zcities.name', 'zcities.region_id', 'zcities.geohash',
                'zcities.optional_city', 'zcities.default_nights', 'zcities.description', 'zcities.small_image',
                'zcities.row_id', 'zcities.country_id', 'zcities.is_disabled', 'zcities.airport_codes',
                'zcities.timezone_id')->with(['latlong', 'country'])->whereIn('id', $city_ids_with_latlong)->get();
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('city_with_langlot', $cities, $expiresAt);
        } else {
            $cities = Cache::get('city_with_langlot');
        }
        return $cities;
    }

    public function getCitiesWithLatLongByKeyId()
    {
        if (!Cache::has('city_with_langlot_by_keyid')) {
            $city_ids_with_latlong = LatLong::pluck('city_id');
            $cities = City::select('zcities.id', 'zcities.name', 'zcities.region_id', 'zcities.geohash',
                'zcities.optional_city', 'zcities.default_nights', 'zcities.description', 'zcities.small_image',
                'zcities.row_id', 'zcities.country_id', 'zcities.is_disabled', 'zcities.airport_codes',
                'zcities.timezone_id')->with(['latlong', 'country'])->whereIn('id',
                $city_ids_with_latlong)->get()->keyBy('id');
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('city_with_langlot_by_keyid', $cities, $expiresAt);
        } else {
            $cities = Cache::get('city_with_langlot_by_keyid');
        }
        return $cities;
    }

    public function getCitiesById()
    {
        if (!Cache::has('cities_by_key_id')) {
            $cities = City::with('country')->get()->keyBy('id');
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('cities_by_key_id', $cities, $expiresAt);
        } else {
            $cities = Cache::get('cities_by_key_id');
        }
        return $cities;
    }

    public function getAirportInformation($airport_code)
    {
        if (!Cache::has('airport_infos')) {
            $airports = CityIata::all()->keyBy('iata_code');
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('airport_infos', $airports, $expiresAt);
        } else {
            $airports = Cache::get('airport_infos');
        }
        return $airports[$airport_code] ?? '';
    }

    public function getAirportInformationColumnWise()
    {
        if (!Cache::has('airport_infos_columns')) {
            $airport_infos_columns = CityIata::selectRaw("CONCAT (COALESCE(city_name,''),'!@',COALESCE(country_name,''),'!@',COALESCE(airport_name,''),'!@',COALESCE(country_code,'')) as columns, iata_code")->pluck('columns',
                'iata_code');
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('airport_infos_columns', $airport_infos_columns, $expiresAt);
        } else {
            $airport_infos_columns = Cache::get('airport_infos_columns');
        }
        return $airport_infos_columns;
    }

    public function getAirlineInfo()
    {
        if (!Cache::has('airline_info')) {
            $airline_info = Airline::pluck("airline_name", "airline_code");
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('airline_info', $airline_info, $expiresAt);
        } else {
            $airline_info = Cache::get('airline_info');
        }
        return $airline_info;
    }

    public function cacheRoutes()
    {
        if (!Cache::has('cache_routes')) {
            $routes = RoutePlan::with('routes', 'to_city.country')->get();
            $cache_routes = $routes->map(function ($val, $key) { // LOOPS ON ROUTE PLANS
                // $route['from_city_name'] = $val['to_city']['name'];
                $city_by_id = $this->getCitiesById();
                $route['from_city_id'] = $val['from_city_id'];
                $route['from_city_name'] = '';
                if ($city_by_id->has($val['from_city_id'])) {
                    $route['from_city_name'] = trim($city_by_id[$val['from_city_id']]->name);
                }
                $route['to_city_id'] = $val['to_city_id'];
                $route['to_city_name'] = trim($val['to_city']['name'] . ', ' . $val['to_city']['country']['name']);
                $route['routes'] = $val['routes']->map(function ($val, $key) { // LOOP ON ROUTES
                    $route_added = 0;
                    if ($val['is_default'] == 1 && $route_added == 0) { // IF ROUTE IS DEFAULT
                        $route_added = 1;
                        $default_route = $val['route_legs']->map(function ($val, $key) { // LOOP ON ROUTE LEGS
                            $city_info = $this->getCityById($val['city_id']);
                            $route['city_id'] = $val['city_id'];
                            $route['city_name'] = $city_info->name;
                            $route['country_id'] = $city_info->country->id;
                            $route['country_name'] = $city_info->country->name;
                            $route['country_code'] = $city_info->country->code;
                            $route['lat'] = $city_info->latlong->lat;
                            $route['lng'] = $city_info->latlong->lng;
                            $route['default_nights'] = $city_info->default_nights;
                            $route['description'] = $city_info->description;
                            $route['small_image'] = $city_info->small_image;
                            return $route;
                        });
                        return $default_route;
                    }
                });
                $route['routes'] = $route['routes']->filter(function ($val, $key) { // IGNORE NULL
                    return $val != null;
                });
                return $route;
            });
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('cache_routes', $cache_routes, $expiresAt);
        } else {
            $cache_routes = Cache::get('cache_routes');
        }
        return $cache_routes;
    }

    public function getViatorDestId($city_id)
    {        
        if (!Cache::has('viator_dest_id_by_key_id')) {     
            $destination = ViatorLocation::all()->keyBy('city_id');                   
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('viator_dest_id_by_key_id', $destination, $expiresAt);
        } else {
            $destination = Cache::get('viator_dest_id_by_key_id');
        }
        
        if (!empty($destination[$city_id])) {
            return $destination[$city_id];
        } else {
            return false;
        }
    }

    public function getCategory($sType)
    {
        if (!Cache::has('activity_category_by_type_' . $sType)) {
            $category = CategoryDef::select('category_from_id as value',
                'category_name as text')->where('category_from', $sType)->get();
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('activity_category_by_type_' . $sType, $category, $expiresAt);
        } else {
            $category = Cache::get('activity_category_by_type_' . $sType);
        }
        return $category;
    }

    /**
     * Returns all per set account options
     *
     * @return mixed
     */
    public function getAllPreSetAccountOptions()
    {
        if (!Cache::has('all_pre_set_account_options')) {
            $perSetAccountConfig = PerSetAccountOptions::select('id as value', 'name as text')->orderBy('name', 'ASC')->get();
            event(new SaveToRedisCache("all_pre_set_account_options", $perSetAccountConfig->toArray()));
        } else {
            $perSetAccountConfig = Cache::get('all_pre_set_account_options');
        }
        return $perSetAccountConfig;
    }

    public function getTemplateList()
    {
        if (!Cache::has('template_list')) {
            $template = Templates::whereStatus('1')->get();
            $templateCache = $template->map(function ($val, $key) {
                $template['value'] = $val['id'];
                $template['text'] = $val['name'];
                return $template;
            });
            $expiresAt = \Carbon\Carbon::now()->addWeeks(50);
            Cache::put('template_list', $templateCache, $expiresAt);
        } else {
            $templateCache = Cache::get('template_list');
        }
        return $templateCache;
    }

    /**
     * Returns timezone list based on cache storage
     *
     * @return mixed
     */
    public function getAllTimezones()
    {
        if (!Cache::has('all_timezones')) {
            $timezones = Timezone::select('id as value', 'name as text', 'utc', 'country_code')->orderBy('name', 'ASC')->get();
            event(new SaveToRedisCache("all_timezones", $timezones->toArray()));
        } else {
            $timezones = Cache::get('all_timezones');
        }
        return $timezones;
    }

    /**
     * Returns cities list based on cache storage
     *
     * @return mixed
     */
    public function getAllCountries()
    {
        if (!Cache::has('all_countries')) {
            $countries = Country::select('id as value', 'name as text', 'country_code')->orderBy('name', 'ASC')->get();
            event(new SaveToRedisCache("all_countries", $countries->toArray()));

        } else {
            $countries = Cache::get('all_countries');
        }
        return $countries;
    }

}