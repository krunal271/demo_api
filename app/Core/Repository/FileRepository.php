<?php

namespace App\Core\Repository;

class FileRepository {

	public function writeJsonFile($file_name,$data){
		$data = json_decode(json_encode($data),TRUE);
		if(!\Storage::disk('local')->exists($file_name)){
			\Storage::disk('local')->put($file_name,json_encode($data));
		}
		return $file_name;
	}
}