<?php

namespace App\Core\Repository;

use App\Models\City;
use App\Models\Products;
use Illuminate\Support\Facades\Cache as Cache;
use Illuminate\Support\Collection;
use App\Core\Repository\StaticRepository;
use App\Core\Repository\CacheRepository;
use App\Core\Repository\ActivityRepository;
use DB;

class ApiRepository{
	protected $staticRepository;
	function __construct(){
		$this->staticRepository = new StaticRepository;
		$this->cacheRepository = new CacheRepository;
		$this->activityRepository = new ActivityRepository;
	}
	public function preference(){
		$response['ageGroups'] = $this->staticRepository->mapAgeGroups();
		$response['transportTypes'] = $this->staticRepository->transportTypes();
		$response['accomodationClasses'] = $this->staticRepository->accomodationClasses();
		$response['nationalities'] = $this->staticRepository->nationalities();
		$response['cabinClass'] = $this->staticRepository->cabinClass();
		$response['activityPreference'] = $this->activityRepository->activityPreference('v'); // Get only viator category by using 'v' parameter
		return $response;
	}

	public function autoRoutes($city_id){
		$default_routes = $this->cacheRepository->cacheRoutes();
		$added_routes = [];
		$from_routes = $default_routes->map(function($val,$key) use ($city_id){ // GET ROUTE DEFINED IN from_city_id
		if($val['from_city_id'] == $city_id)
		{
			return $val;
		}
		})->filter(function($val,$key){
			return $val != null;
		});
		$added_routes = array_column($from_routes->toArray(), 'to_city_id');
		$to_routes = $default_routes->map(function($val,$key) use ($city_id,$added_routes){ // GET ROUTE DEFINED
			if($val['to_city_id'] == $city_id && !in_array($val['from_city_id'], $added_routes)){
				$route['from_city_id'] = $val['to_city_id'];
				$route['from_city_name'] = $val['to_city_name'];
				$route['to_city_id'] = $val['from_city_id'];
				$route['to_city_name'] = $val['from_city_name'];
				$route['routes'][0] = $val['routes'][0]->reverse()->values();
				return $route;
			}
		})->filter(function($val,$key){
			return $val != null;
		});
		return $from_routes->concat($to_routes);
	}
	public function mapData($origin_city,$destination_city){
		$response['city_lat_long']=$this->cacheRepository->getCitiesWithLatLong();
		$response['mapData'] = $this->cacheRepository->getMapRoute($origin_city,$destination_city);
		return $response;
	}
	public function findNearestCities($center_latitude,$center_logitude,$distance,$zoom_level){
		if($zoom_level == 6){
			$level = "1,2";
		}
		else if($zoom_level == 7 || $zoom_level < 9 ){
			$level = "1,2,3";
		}
		else if($zoom_level == 9 || $zoom_level > 9){
			$level = "1,2,3,4";
		}
		$response['cities'] = DB::select("SELECT city.id,city.name,city.level,langlat.lat,langlat.lng,city.description,city.small_image,country.name as coutry_name,langlat.distance_in_km
			from (
				select city_id,lng,lat,111.045 * DEGREES(ACOS(COS(RADIANS($center_latitude))
					 * COS(RADIANS(lat))
					 * COS(RADIANS(lng) - RADIANS($center_logitude))
					 + SIN(RADIANS($center_latitude))
					 * SIN(RADIANS(lat)))) as distance_in_km
				from zcitieslatlang
			) langlat INNER JOIN zcities city on city.id = langlat.city_id AND langlat.distance_in_km <= $distance
				INNER JOIN zcountries country on country.id = city.country_id
			WHERE city.level IN ($level)
			ORDER BY langlat.distance_in_km ASC");
		return $response;

	}
	public function findRoutes($origin_city,$destination_city){
		$default_routes = $this->cacheRepository->cacheRoutes();
		$added_routes = [];
		$from_routes = $default_routes->map(function($val,$key) use ($origin_city,$destination_city){ // GET ROUTE DEFINED IN from_city_id
		if($val['from_city_id'] == $origin_city && $val['to_city_id'] == $destination_city)
		{
			return $val;
		}
		})->filter(function($val,$key){
			return $val != null;
		});
		$added_routes = array_column($from_routes->toArray(), 'to_city_id');
		$response = $from_routes;
		if(empty($added_routes)){
			$to_routes = $default_routes->map(function($val,$key) use ($origin_city,$destination_city,$added_routes){ // GET ROUTE DEFINED
			if($val['to_city_id'] == $origin_city && $val['from_city_id'] == $destination_city && !in_array($val['from_city_id'], $added_routes)){
				$route['from_city_id'] = $val['to_city_id'];
				$route['from_city_name'] = $val['to_city_name'];
				$route['to_city_id'] = $val['from_city_id'];
				$route['to_city_name'] = $val['from_city_name'];
				$route['routes'][0] = $val['routes'][0]->reverse()->values();
				return $route;
			}
			})->filter(function($val,$key){
				return $val != null;
			});
			$response = $from_routes->concat($to_routes);
		}
		return $response->first();
	}

	
    public function getProducts() {
	    $products = Products::select("id as value","name as text")->orderBy('name','ASC')->get();
        return $products;
    }



}