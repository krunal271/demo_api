<?php

namespace App\Core\Service;

use App\Contracts\ApiContract;
use App\Core\Repository\AccountConfigurationRepository;
use App\Core\Repository\AccountNotesRepository;
use App\Core\Repository\AuthRepository;
use App\Core\Repository\BackendConfigurationRepository;
use App\Core\Repository\CustomerRepository;
use App\Core\Repository\GeneralRepository;
use App\Core\Repository\GeoDataRepository;
use App\Core\Repository\InventoryMarketPlaceRepository;
use App\Core\Repository\StaticRepository;
use App\Core\Repository\ApiRepository;
use App\Core\Repository\FrontendConfigurationRepository;
use App\Core\Repository\UserManagementRepository;
use App\Core\Repository\TaskRepository;


class ApiService implements ApiContract
{

    protected $staticRepository;
    protected $apiRepository;
    protected $generalRepository;
    protected $frontendConfigurationRepository;
    protected $backendConfigurationRepository;
    protected $geoDataRepository;
    protected $inventoryMarketPlaceRepository;
    protected $userManagementRepository;
    protected $accountConfigurationRepository;
    protected $accountNotesRepository;
    protected $authRepository;
    protected $customerRepository;

    public function currencies()
    {
        $this->staticRepository = new StaticRepository;
        return $response = $this->staticRepository->currencies();
    }

    public function preference()
    {
        $this->apiRepository = new ApiRepository;
        return $response = $this->apiRepository->preference();
    }

    public function autoRoutes($origin_city)
    {
        $this->apiRepository = new ApiRepository;
        return $response = $this->apiRepository->autoRoutes($origin_city);
    }

    public function mapData($origin_city, $destination_city)
    {
        $this->apiRepository = new ApiRepository;
        return $response = $this->apiRepository->mapData($origin_city, $destination_city);
    }

    public function getAllCities()
    {
        $this->apiRepository = new ApiRepository;
        return $response = $this->apiRepository->getAllCities();
    }

    public function findNearestCities($center_latitude, $center_logitude, $distance, $zoom_level)
    {
        $this->apiRepository = new ApiRepository;
        return $response = $this->apiRepository->findNearestCities($center_latitude, $center_logitude, $distance,
            $zoom_level);
    }

    public function findRoutes($origin_city, $destination_city)
    {
        $this->apiRepository = new ApiRepository;
        return $response = $this->apiRepository->findRoutes($origin_city, $destination_city);
    }

    public function addLicenseeDetail($request)
    {
        $this->generalRepository = new GeneralRepository;
        return $response = $this->generalRepository->addLicenseeDetail($request);
    }

    public function editLicenseeDetail($request)
    {
        $this->generalRepository = new GeneralRepository;
        return $response = $this->generalRepository->editLicenseeDetail($request);
    }

    public function licenseeList()
    {
        $this->generalRepository = new GeneralRepository;
        return $response = $this->generalRepository->licenseeList();
    }

    public function licenseeDetail($request)
    {
        $this->generalRepository = new GeneralRepository;
        return $response = $this->generalRepository->licenseeDetail($request);
    }


    public function getAllPreSetAccountOptions()
    {
        $this->generalRepository = new GeneralRepository;
        return $response = $this->generalRepository->getAllPreSetAccountOptions();
    }

    public function getProducts()
    {
        $this->apiRepository = new ApiRepository;
        return $response = $this->apiRepository->getProducts();
    }

    public function addFrontendConfiguration($request)
    {
        $this->frontendConfigurationRepository = new FrontendConfigurationRepository;
        return $response = $this->frontendConfigurationRepository->addFrontendConfiguration($request);
    }

    public function editFrontendConfiguration($request)
    {
        $this->frontendConfigurationRepository = new FrontendConfigurationRepository;
        return $response = $this->frontendConfigurationRepository->editFrontendConfiguration($request);
    }

    public function frontendConfigurationDetail($request)
    {
        $this->frontendConfigurationRepository = new FrontendConfigurationRepository;
        return $response = $this->frontendConfigurationRepository->frontendConfigurationDetail($request);
    }

    public function backendServices()
    {
        $this->backendConfigurationRepository = new BackendConfigurationRepository;
        return $response = $this->backendConfigurationRepository->backendServices();
    }

    public function addBackendConfiguration($request)
    {
        $this->backendConfigurationRepository = new BackendConfigurationRepository;
        return $response = $this->backendConfigurationRepository->addBackendConfiguration($request);
    }

    public function editBackendConfiguration($request)
    {
        $this->backendConfigurationRepository = new BackendConfigurationRepository;
        return $response = $this->backendConfigurationRepository->editBackendConfiguration($request);
    }

    public function backendConfigurationDetail($request)
    {
        $this->backendConfigurationRepository = new BackendConfigurationRepository;
        return $response = $this->backendConfigurationRepository->backendConfigurationDetail($request);
    }

    public function addGeoData($request)
    {
        $this->geoDataRepository = new GeoDataRepository;
        return $response = $this->geoDataRepository->addGeoData($request);
    }

    public function editGeoData($request)
    {
        $this->geoDataRepository = new GeoDataRepository;
        return $response = $this->geoDataRepository->editGeoData($request);
    }

    public function viewGeoData($request)
    {
        $this->geoDataRepository = new GeoDataRepository;
        return $response = $this->geoDataRepository->viewGeoData($request);
    }

    public function addInventoryMarketPlace($request)
    {
        $this->inventoryMarketPlaceRepository = new InventoryMarketPlaceRepository;
        return $response = $this->inventoryMarketPlaceRepository->addInventoryMarketPlace($request);
    }

    public function editInventoryMarketPlace($request)
    {
        $this->inventoryMarketPlaceRepository = new InventoryMarketPlaceRepository;
        return $response = $this->inventoryMarketPlaceRepository->editInventoryMarketPlace($request);
    }

    public function viewInventoryMarketPlace()
    {
        $this->inventoryMarketPlaceRepository = new InventoryMarketPlaceRepository;
        return $response = $this->inventoryMarketPlaceRepository->viewInventoryMarketPlace();
    }

    public function getAllRoles()
    {
        $this->userManagementRepository = new UserManagementRepository;
        return $response = $this->userManagementRepository->getAllRoles();
    }

    public function addCmsUser($request)
    {
        $this->userManagementRepository = new UserManagementRepository;
        return $response = $this->userManagementRepository->addCmsUser($request);
    }

    public function editCmsUser($request)
    {
        $this->userManagementRepository = new UserManagementRepository;
        return $response = $this->userManagementRepository->editCmsUser($request);
    }

    public function viewAccountConfiguration($request)
    {
        $this->accountConfigurationRepository = new AccountConfigurationRepository;
        return $response = $this->accountConfigurationRepository->viewAccountConfiguration($request);
    }

    public function addAccountConfiguration($request)
    {
        $this->accountConfigurationRepository = new AccountConfigurationRepository;
        return $response = $this->accountConfigurationRepository->addAccountConfiguration($request);
    }

    public function editAccountConfiguration($request)
    {
        $this->accountConfigurationRepository = new AccountConfigurationRepository;
        return $response = $this->accountConfigurationRepository->editAccountConfiguration($request);
    }

    public function addAccountNotes($request)
    {
        $this->accountNotesRepository = new AccountNotesRepository;
        return $response = $this->accountNotesRepository->addAccountNotes($request);
    }

    public function editAccountNotes($request)
    {
        $this->accountNotesRepository = new AccountNotesRepository;
        return $response = $this->accountNotesRepository->editAccountNotes($request);
    }

    public function salesRepresentativeList()
    {
        $this->accountNotesRepository = new AccountNotesRepository;
        return $response = $this->accountNotesRepository->salesRepresentativeList();
    }

    public function loginCmsUser($request)
    {
        $this->authRepository = new AuthRepository;
        return $response = $this->authRepository->loginCmsUser($request);
    }

    public function cmsUserList($request)
    {
        $this->userManagementRepository = new UserManagementRepository;
        return $response = $this->userManagementRepository->cmsUserList($request);
    }

    public function rolesPermissionsList($request)
    {
        $this->userManagementRepository = new UserManagementRepository;
        return $response = $this->userManagementRepository->rolesPermissionsList($request);
    }

    public function permissionsList($request)
    {
        $this->userManagementRepository = new UserManagementRepository;
        return $response = $this->userManagementRepository->permissionsList($request);
    }

    public function cmsUserDetail($request) {
        $this->userManagementRepository = new UserManagementRepository;
        return $response = $this->userManagementRepository->cmsUserDetail($request);
    }

    public function customerList($request)
    {
        $this->customerRepository = new CustomerRepository;
        return $response = $this->customerRepository->customerList($request);
    }

    public function viewCustomer($request)
    {
        $this->customerRepository = new CustomerRepository;
        return $response = $this->customerRepository->viewCustomer($request);
    }

    public function addCustomer($request)
    {
        $this->customerRepository = new CustomerRepository;
        return $response = $this->customerRepository->addCustomer($request);
    }

    public function editCustomer($request)
    {
        $this->customerRepository = new CustomerRepository;
        return $response = $this->customerRepository->editCustomer($request);
    }

}