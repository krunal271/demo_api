<?php

namespace App\Core\Service;

use App\Contracts\TaskContract;
use App\Core\Repository\AccountConfigurationRepository;
use App\Core\Repository\AccountNotesRepository;
use App\Core\Repository\AuthRepository;
use App\Core\Repository\BackendConfigurationRepository;
use App\Core\Repository\CustomerRepository;
use App\Core\Repository\GeneralRepository;
use App\Core\Repository\GeoDataRepository;
use App\Core\Repository\InventoryMarketPlaceRepository;
use App\Core\Repository\StaticRepository;
use App\Core\Repository\ApiRepository;
use App\Core\Repository\FrontendConfigurationRepository;
use App\Core\Repository\UserManagementRepository;
use App\Core\Repository\TaskRepository;


class TaskService implements TaskContract
{
    public function addTaskData($request) {
       $this->TaskRepository = new TaskRepository;
       return $this->TaskRepository->addTaskData($request);
   }
   public function editTaskStatus($request) {
       $this->TaskRepository = new TaskRepository;
       return $this->TaskRepository->editTaskStatus($request);
   }
   public function getTaskList($request) {

       $this->TaskRepository = new TaskRepository;
       return $this->TaskRepository->getTaskList($request);
   }

   public function addToComplete($request) {

       $this->TaskRepository = new TaskRepository;
       return $this->TaskRepository->addToComplete($request);
   }
   public function deleteTask($request) {

       $this->TaskRepository = new TaskRepository;
       return $this->TaskRepository->deleteTask($request);
   }

   public function addtoSnooze($request) {

       $this->TaskRepository = new TaskRepository;
       return $this->TaskRepository->addtoSnooze($request);
   }
 }