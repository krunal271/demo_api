<?php

namespace App\Core\Service;

use App\Contracts\ActivityContract;
use App\Core\Repository\ActivityRepository;

class ActivityService implements ActivityContract
{
	public function getViatorActivityDetail($request) {
        $this->ActivityRepository = new ActivityRepository;
        return $response = $this->ActivityRepository->getViatorActivityDetail($request);
    }

    public function getViatorHotelList($request) {
        $this->ActivityRepository = new ActivityRepository;
        return $response = $this->ActivityRepository->getViatorHotelList($request);
    }
    public function listActivities($fileName){
    	$this->ActivityRepository = new ActivityRepository;
    	return $response = $this->ActivityRepository->listActivities($fileName);
    }
}