<?php
namespace App\Core\Service;
use App\Contracts\CustomerContract;
use App\Core\Repository\CustomerRepository;
class CustomerService implements CustomerContract{

	public function customers($request){
		$customerRepository = new CustomerRepository;
		return $customerRepository->customers($request);
	}
	public function getCustomer($user_data){
		$customerRepository = new CustomerRepository;
		return $customerRepository->getCustomer($user_data);
	}
}