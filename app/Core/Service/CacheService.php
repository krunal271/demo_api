<?php


namespace App\Core\Service;

use App\Contracts\CacheContract;
use App\Core\Repository\CacheRepository;

class CacheService implements CacheContract
{
    protected $cacheRepository;

    function __construct()
    {
        $this->cacheRepository = new CacheRepository;
    }

    public function getAllCities()
    {
        return $response = $this->cacheRepository->getAllCities();
    }

    public function cacheRoutes()
    {
        return $response = $this->cacheRepository->cacheRoutes();
    }

    public function getCityById($id)
    {
        return $response = $this->cacheRepository->getCityById($id);
    }
    public function getRouteCityById($id){
     return $response = $this->cacheRepository->getRouteCityById($id);   
    }
    public function getAllTimezones()
    {
        return $response = $this->cacheRepository->getAllTimezones();
    }

    public function getAllCountries()
    {
        return $response = $this->cacheRepository->getAllCountries();
    }

}