<?php
namespace App\Import;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\Models\City;
use App\Models\CitiesLatLong;
use App\Models\Country;

class CityImport implements ToModel, WithHeadingRow{
  
  public function model(array $row)
  {
    if(isset($row['city_id']) && isset($row['country_id'])) {
      $data['id'] = $row['city_id'];
      $data['name'] = $row['city_name'];
      $data['level'] = 0;
      //$data['region_id'] = $row['city_id'];
      $data['geohash'] = $row['geohash'];
      $data['optional_city'] = $row['city_id'];
      $data['default_nights'] = $row['default_nights'];
      $data['description'] = $row['city_description'];
      $data['small_image'] = '';
      $data['row_id'] = 0;
      $data['country_id'] = $row['country_id'];
      $data['created_by'] = 0;
      $data['is_disabled'] = ($row['is_disabled'] == 0)?0:1;
      //$data['airport_codes'] = '';
      $data['timezone_id'] = 0;
      $data['domain_id'] = 0;
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['updated_at'] = date('Y-m-d H:i:s');
      City::create($data);
    }
    
    //dd($data);
    /*
      $countryDetail = Country::where('name', $row['country_name'])->first();
      $country_id = $countryDetail['id'] ?? 0;

    	$cityDetail = City::where(['name'=>$row['city_name'],'country_id'=>$country_id])->first();
      $exist_city = $cityDetail['id'] ?? 0; 
       
      if($row['is_disabled'] !=''){
          $disable = $row['is_disabled']; 
      }else{
          $disable = 0;   
      }

      if($row['city_name'] == 'Edmonton' || $row['city_name'] == 'Karachi'){
        $level = 0; 
      }else{
        $level = $row['optional_city'];
      }

      $data = array(
        'name'  => $row['city_name'], 
        'country_id' => $country_id,
        'region_id' => $row['region_id'],
        'geohash' => $row['geohash'],
        'level' => $level,
        'default_nights' => $row['default_nights'],
        'description' => $row['city_description'],
        'is_disabled' => $disable,
        'airport_codes' => $row['airport_codes']
      );

      if($exist_city > 0){
        // dd($exist_city);
        // City::where('id', $exist_city)->update($data);
        $lastId = $exist_city;  
      } else{
        $lastId = City::insertGetId($data);
      }

      if($lastId > 0){
        $latlngDetail = CitiesLatLong::where('city_id',$lastId)->first();
        $latlng_id = $latlngDetail['id'] ?? 0;

        $latLng_data = array(
           'city_id'  => $lastId, 
           'lat' => $row['longitude'],
           'lng' => $row['latitude']
        );

        if($latlng_id > 0){
          // CitiesLatLong::where('id', $latlng_id)->update($latLng_data);
        }else{
          CitiesLatLong::insertGetId($latLng_data);
        }
      }*/

  }
}