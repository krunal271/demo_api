<?php
namespace App\Import;

use App\Core\Repository\CountryRepository;

use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\Models\Country;


class CountryImport implements ToModel, WithHeadingRow{
  protected $countryRepository;
  function __construct(){
    $this->countryRepository = new CountryRepository;
  }
  public function model(array $row)
  {
    $countryDetail = Country::where('name', $row['country_name'])->first();

    $exist_country = $countryDetail['id'] ?? 0;
    if($row['country_code']) {
        $data = array(
          'id'=> $row['country_id'],
          'country_code'  => $row['country_code'], 
          'name' => $row['country_name'],
          'region_id' => $row['region_id'],
          'iso_numeric_code' => $row['iso_numeric_code'],
          'code' => $row['iso_2_letter_code'],
          'iso_3_letter_code' => $row['iso_3_letter_code'],
          'region_id' => $this->countryRepository->findRegionId($row['region_name']),
          'show_on_eroam' => $row['show_on_eroam']
        );
        if($exist_country > 0){
          Country::where('id', $exist_country)->update($data);
          $lastId = $exist_country;  
        }else{
          $lastId = Country::create($data);
        }
    }
  }
}