<?php
namespace App\Import;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\Models\City;
use App\Models\VoyegerCities;

class VoyegerCityImport implements ToModel, WithHeadingRow{
  
    public function model(array $row)
    {
        if($row['lang_code'] == 'EN')
        {
            $city_id = 0;
            $country_id = 0;
            $city_data = City::select('id','country_id')->where('name','LIKE','%'.$row['city_name'].'%')->first();
            
            if(!empty($city_data))  
            {
                $city_id = $city_data->id ?? 0;
                $country_id = $city_data->country_id ?? 0;
            }           

            $data = [
                'eroam_city_id' => $city_id,
                'eroam_country_id' => $country_id,
                'city_code' => $row['city_code'],
                'city_name' => $row['city_name'],
                'iso_code' => $row['isocode'],
                'state_code' => $row['state_code'] ?? NULL,
            ];
           
            VoyegerCities::updateOrCreate(['city_code'=> $row['lang_code']],$data);
            dd("inn");
        }
    }
}