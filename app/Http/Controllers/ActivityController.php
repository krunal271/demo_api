<?php

namespace App\Http\Controllers;
use App\Contracts\SearchContract;
use App\Contracts\ActivityContract;
use Illuminate\Http\Request;

class ActivityController extends Controller {

	public function prefetchActivity(Request $request,SearchContract $searchObj) {
		$origin_city 				= $request->input('origin_city')['value'] ? $request->input('origin_city')['value'] : $request->input('city_id');		
		$checkin_date 				= $request->input('check_in_date');
		$search_preference			= [];
		$pax_info 					= [];
		/* The below things are static at now, I will changed these things once we create array for pax information from frontend (start)*/
		$pax_info['total_adult'] 	= 2;
		$pax_info['total_child'] 	= 0;
		$pax_info['total_infant'] 	= 0;
		/*$pax_info['child_age'][0] 	= 0;
		$pax_info['child_age'][1] 	= 0;
		$pax_info['infant_age'][0] 	= 0;
		$pax_info['infant_age'][1] 	= 0;*/
		/* The below things are static at now, I will changed these things once we create array for pax information from frontend (end)*/
		$pax_information  			= $this->setAdultChildInfoName($pax_info);

		if($request->has('activity_category_id')) {
			$search_preference['activity_category_id'] = $request->input('activity_category_id');
		}

		$response = $searchObj->searchActivities($origin_city,$checkin_date,$search_preference,$pax_information);
		return ['code'=>200,'data'=>$response,'message'=>"Activity"];
	}

	public function viatorActivityDetail(Request $request,ActivityContract $actObj)
	{
		$code 			= $request->input('code');
		$data['code'] 	= $code;
		$response   	= $actObj->getViatorActivityDetail($data);
		return ['code'=>200,'data'=>$response,'message'=>"Activity"];
	}
	public function listActivities(Request $request,ActivityContract $activityObj){
		$file = $request->input('activity_file_name');
		$response = $activityObj->listActivities($file);
		return $response;
	}

	public function setAdultChildInfoName($pax_info)
	{
		$pax_information = [];	
		for($i = 0; $i < $pax_info['total_adult'] ; $i++)
		{
			$j = $i + 1;
			$adult = [
				'name' => 'Adult '.$j,
				'age'  => 33
			];
			array_push($pax_information,$adult);
		}

		for($i = 0; $i < $pax_info['total_child'] ; $i++)
		{
			$j = $i + 1;
			$child = [
				'name' => 'Child '.$j,
				'age'  => $pax_info['child_age'][$i]
			];
			array_push($pax_information,$child);
		}

		for($i = 0; $i < $pax_info['total_infant'] ; $i++)
		{
			$j = $i + 1;
			$infant = [
				'name' => 'Infant '.$j,
				'age'  => $pax_info['infant_age'][$i]
			];
			array_push($pax_information,$infant);
		}

		return $pax_information;
	}

	public function getViatorHotelList(Request $request,ActivityContract $actObj)
	{
		$data['code'] 	= $request->input('code');
		$response   	= $actObj->getViatorHotelList($request);
		return ['code'=>200,'data'=>$response,'message'=>"Activity"];
	}
}