<?php

namespace App\Http\Controllers;

use App\Contracts\ApiContract;
use App\Contracts\SearchContract;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class BackendConfigurationController extends Controller
{
    /**
     * Return list of backend services
     *
     * @param ApiContract $data
     * @return array
     */
    public function backendServices(ApiContract $data)
    {
        $response = $data->backendServices();
        if ($response['code'] === 200) {
            return $this->successFailResponse('Backend Services', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * add backend configuration
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws ValidationException
     */
    public function addBackendConfiguration(Request $request, ApiContract $data)
    {
        $this->validateRequest('backendConfiguration');
        $response = $data->addBackendConfiguration($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Backend Configuration Added.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * edit backend configuration
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws ValidationException
     */
    public function editBackendConfiguration(Request $request, ApiContract $data)
    {
        $this->validateRequest('backendConfiguration');
        $response = $data->editBackendConfiguration($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Backend Configuration Edited.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * Returns the details of back-end configuration
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws ValidationException
     */
    public function backendConfigurationDetail(Request $request, ApiContract $data)
    {
        $this->validateRequest('backendConfigurationDetail');
        $response = $data->backendConfigurationDetail($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Backend Configuration Details.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }
}