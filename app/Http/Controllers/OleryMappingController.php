<?php

namespace App\Http\Controllers;

use App\Contracts\SearchContract;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class OleryMappingController extends Controller
{
    /**
     * Returns the response data of olery
     *
     * @param SearchContract $oleryObj
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    public function getOleryIdFromExpediaId(SearchContract $oleryObj, Request $request)
    {
        $this->validateRequest('oleryMapping');
        $response = $oleryObj->searchOleryId($request['expedia_id']);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Data found.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }
}