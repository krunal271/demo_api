<?php

namespace App\Http\Controllers;

use App\Contracts\ApiContract;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;


class AccountConfigurationController extends Controller
{

    /***
     * Returns the details of account configuration
     *
     * @param ApiContract $obj
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function viewAccountConfiguration(ApiContract $obj, Request $request)
    {
        $this->validateRequest('viewAccountConfigurationValidation');
        $response = $obj->viewAccountConfiguration($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Account Configuration Details.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /***
     * Add and returns account configuration details
     *
     * @param ApiContract $obj
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function addAccountConfiguration(ApiContract $obj, Request $request)
    {
        $this->validateRequest('accountConfigurationValidation');
        $response = $obj->addAccountConfiguration($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Account Configuration Added.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /***
     * Edit and returns account configuration details
     *
     * @param ApiContract $obj
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function editAccountConfiguration(ApiContract $obj, Request $request)
    {
        $this->validateRequest('accountConfigurationValidation');
        $response = $obj->editAccountConfiguration($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Account Configuration Edited.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }

    }
}