<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\SearchContract;
use App\Contracts\TransportContract;

class TransportController extends Controller
{

    public function searchTransport(Request $request, SearchContract $searchObj)
    { // FOR MYSTIFLY
        $transport_type = $request->input('transport_type');
        $origin_city = $request->input('city_id');
        $destination_city = $request->input('destination_city');
        $checkin_date = $request->input('check_in_date');
        $travel_date = $request->input('travel_date');
        $pax_info = 2;//$request->input('pax_info');
        // $transport_type = '';
        // $origin_city = 375;
        // $destination_city = 394;
        // $checkin_date = '2019-07-25';
        // $travel_date=''

        $response = $searchObj->searchTransport($origin_city, $destination_city, $checkin_date,$travel_date, $transport_type,$pax_info);

        return ['code' => 200, 'data' => $response, 'message' => "Transport"];
    }

    public function listTransport(Request $request,TransportContract $transportObj){
        $transport_type = $request->input('transport_type');
        $origin_city = $request->input('city_id');
        $destination_city = $request->input('destination_city');
        $checkin_date = $request->input('check_in_date');
        $travel_date = $request->input('travel_date');
        $pax_info = 2;
		$response = $transportObj->getBusList($origin_city, $destination_city, $checkin_date,$travel_date, $transport_type,$pax_info);
		return ['code' => 200, 'data' => $response, 'message' => "Transport"];               
    }  

}

