<?php
namespace App\Http\Controllers;
use App\Contracts\FeedContract;
use Illuminate\Http\Request;

class FeedController extends Controller
{   
	public function cityFeed(FeedContract $feedObj){
		$file = base_path().config('constants.CITYFEEDPATH');
		$feedObj->feedCities($file);
        return ['code' => 200,'message' => 'Cities Import Successfully.'];
	}
    public function countryFeed(FeedContract $feedObj){
		$file = base_path().config('constants.COUNTRYFEEDPATH');
		$feedObj->feedCountries($file);
		return ['code' => 200,'message' => 'Countries Import Successfully'];
    }

    public function callReadCityCSV(FeedContract $feedObj){
		$file = base_path().config('constants.VOYEGERCITYFEEDPATH');
		$feedObj->feedVoyegerCities($file);
        return ['code' => 200,'message' => 'Cities Import Successfully.'];
	}
	
	public function callStoreHotels(FeedContract $feedObj) {
		$file = base_path().config('constants.VOYEGERHOTELSFEEDPATH');
		$feedObj->feedVoyegerHotels($file);
		return ['code' => 200,'message' => 'Cities Import Successfully.'];
	}
}