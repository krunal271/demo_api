<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\TourContract;

class ToursController extends Controller
{
	public function search_tours(TourContract $tour,Request $request)
	{
		$data =[];
		$data['originCityId'] = $request->input('originCityId');
		$data['destinationCityId'] = $request->input('destinationCityId');
		$data['searchType'] = $request->input('searchType');
		$data['allCity'] = $request->input('allCity');
		$data['searchKeyword'] = $request->input('searchKeyword');
		$data['destination'] = $request->input('destination');
		$data['tourLength'] = $request->input('tourLength');
		$data['tourPrice'] = $request->input('tourPrice');
		$data['tourExperience'] = $request->input('tourExperience');
		$data['tourSupplier'] = $request->input('tourSupplier');
		
		$tours = $tour->search_tour($data);
		return ['code' => 200, 'data' => $tours, 'message' => "Tours"];
	}
	public function tour_details(TourContract $tour,Request $request)
	{
		$tour_id = $request->input('tour_id');
		$tour = $tour->tour_details($tour_id);
		return ['code' => 200, 'data' => $tour, 'message' => "Tour Details"];
	}
	
	public function storeTours(TourContract $tour,$provider)
	{
		if (empty($provider)) {
			return ['code' => 200, 'message' => "Invalid provider"];
		} else {
			$checkProvider = $tour->checkProvider($provider);
			if ($checkProvider) {
				if ($provider == 'INT') {
					$tour = $tour->storeTour($provider);
				} else if($provider == 'TTC') {
					$tour = $tour->storeTour($provider);
				}
			} else {
				return ['code' => 300, 'message' => "Invalid provider"];
			}
		}
	}

	public function getTourDepartures(TourContract $tour,Request $request)
	{
		$provider = $request->input('provider');
		$brand = $request->input('brand');
		$tourCode = $request->input('tourCode');
		$tourOptionCode = $request->input('tourOptionCode') ?? '';
		
		if (empty($provider)) {
			return ['code' => 200, 'message' => "Invalid provider"];
		} else {
			$checkProvider = $tour->checkProvider($provider);
			if ($checkProvider) {
				if ($provider == 'INT') {
					$departures = $tour->getDepartures($provider,$brand,$tourCode,$tourOptionCode);
				} else if($provider == 'TTC') {
					$departures = $tour->getDepartures($provider,$brand,$tourCode,$tourOptionCode);
				}
				return ['code' => 200, 'data'=> $departures,'message' => "Departures list"];
			} else {
				return ['code' => 300, 'message' => "Invalid provider"];
			}
		}
	}
}