<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\TaskContract;
class TaskController extends Controller
{

    public function addTaskData(Request $request, TaskContract $data){
        $this->validateRequest('addTaskData');
        $request['user_id'] = $request->user()->id;
        $response = $data->addTaskData($request->all());
        if ($response['code'] === 200) {
            return ['code' => 200, 'data' => $response['data'], 'message' => $response['message']];
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code'  ]) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
	}

	public function editTask(Request $request, TaskContract $data){
		$this->validateRequest('editTaskStatus');
        $response = $data->editTaskStatus($request->all());
        if ($response['code'] === 200) {
            return ['code' => 200, 'data' => $response['data'], 'message' => "Task Status"];
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
	}

    public function getTaskList(Request $request,TaskContract $data){
        $response = $data->getTaskList($request->all());
        if ($response['code'] === 200) {
            return $this->successFailResponse('Task list', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    public function addToComplete(Request $request,TaskContract $data){
        $response = $data->addToComplete($request->all());

        if ($response['code'] === 200) {
            return ['code' => 200, 'data' => $response['data'], 'message' => "Task Added To Complete List "];
//            return $this->successFailResponse('Task Added Successfully!', $response['data'], 200);
        } else   {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }
    public function deleteTask(Request $request, TaskContract $data)
    {
        $response = $data->deleteTask($request->all());
        if ($response['code'] === 200) {
            return ['code' => 200, 'data' => $response['data'], 'message' => "Task Deleted Successfully!"];
//            return $this->successFailResponse('Task Added Successfully!', $response['data'], 200);
        } else   {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    public function addtoSnooze(Request $request, TaskContract $data)
    {
        $response = $data->addtoSnooze($request->all());
        if ($response['code'] === 200) {
            return ['code' => 200, 'data' => $response['data'], 'message' => "Task Added To Snooze Successfully!"];
//            return $this->successFailResponse('Task Added Successfully!', $response['data'], 200);
        } else   {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }
    
}