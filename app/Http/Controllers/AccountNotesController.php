<?php

namespace App\Http\Controllers;

use App\Contracts\ApiContract;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AccountNotesController extends Controller
{

    /**
     * Add and returns account notes
     *
     * @param ApiContract $obj
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function addAccountNotes(ApiContract $obj, Request $request)
    {
        $this->validateRequest('accountNotesValidation');
        $response = $obj->addAccountNotes($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Account Notes Added.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * Edit and returns account notes
     *
     * @param ApiContract $obj
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    public function editAccountNotes(ApiContract $obj, Request $request)
    {
        $this->validateRequest('accountNotesValidation');
        $response = $obj->editAccountNotes($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Account Notes Edited.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * Returns list of sales representative
     *
     * @param ApiContract $obj
     * @return mixed
     */
    public function salesRepresentativeList(ApiContract $obj)
    {
        $response = $obj->salesRepresentativeList();
        if ($response['code'] === 200) {
            return $this->successFailResponse('Sales Representative List.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }
}