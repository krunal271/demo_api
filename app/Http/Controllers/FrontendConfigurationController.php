<?php

namespace App\Http\Controllers;

use App\Contracts\ApiContract;
use App\Contracts\SearchContract;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class FrontendConfigurationController extends Controller
{
    /**
     * add front-end configuration
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws ValidationException
     */
    public function addFrontendConfiguration(Request $request, ApiContract $data)
    {
        $this->validateRequest('addFrontendConfiguration');
        $response = $data->addFrontendConfiguration($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Frontend Configuration Added.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * edit front-end configuration
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws ValidationException
     */
    public function editFrontendConfiguration(Request $request, ApiContract $data)
    {
        $this->validateRequest('editFrontendConfiguration');
        $response = $data->editFrontendConfiguration($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Frontend Configuration Edited.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * Returns list of templates
     *
     * @param SearchContract $obj
     * @return array
     */
    public function templateList(SearchContract $obj)
    {
        $response = $obj->searchTemplate();
        if ($response['code'] === 200) {
            return $this->successFailResponse('Templates Lists', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * Returns the details of front-end configuration
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws ValidationException
     */
    public function frontendConfigurationDetail(Request $request, ApiContract $data)
    {
        $this->validateRequest('frontendConfigurationDetail');
        $response = $data->frontendConfigurationDetail($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Frontend Configuration Details.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }
}