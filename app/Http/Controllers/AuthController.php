<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\ApiContract;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{

    /**
     * check and returns the authentication of cms-user
     *
     * @param ApiContract $apiObj
     * @return mixed
     * @throws ValidationException
     */
    public function loginCmsUser(ApiContract $apiObj, Request $request)
    {
        $this->validateRequest('login');
        $response = $apiObj->loginCmsUser($request);
        if ($response['code'] === 200) {
            return ['message' => 'Login Success.', 'data' => $response['data'], 'code' => 200];
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            return $this->successFailResponse($message, null, 400);
        }
    }
}