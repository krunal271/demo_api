<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Contracts\ItineraryContract;
use Log;

class ItineraryController extends Controller{
	
	public function saveItinerary(Request $request,ItineraryContract $itinerary){
		$data['email'] = $request->input('email');
		$data['name'] = $request->input('name');
		$data['phone_number'] = $request->input('phone_number');
		$data['itinerary_json'] = json_decode($request->input('itinerary_json'),TRUE);
		$data['selected_routes'] = json_decode($request->input('selected_routes'),TRUE);
		$data['agent_id'] = $request->input('agent_id');
		$data['is_update'] = $request->input('is_update');
		$data['itinerary_code'] = $request->input('itinerary_code');
		
		return $itinerary->saveItineries($data);
	}
	public function getSavedItinerary(Request $request,ItineraryContract $itinerary){
		$data['agent_id'] = $request->user()->id;
		return $itinerary->getSavedItinerary($data);
	}
	public function getPendingBooking(Request $request,ItineraryContract $itinerary){
		$data['itinerary_code'] = $request->input('itinerary_code');
		return $itinerary->getPendingBooking($data);	
	}
}