<?php

namespace App\Http\Controllers;

use App\Models\CmsUser;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    public static $VALIDATION_FAILED_HTTP_CODE = Response::HTTP_BAD_REQUEST;
    public static $UNAUTHORIZED_USER = Response::HTTP_UNAUTHORIZED;

    /**
     * For handle server-side validations
     *
     * @param $api
     * @throws ValidationException
     */
    public function validateRequest($api)
    {
        $rules = config("validations.{$api}.rules");
        if (!$rules) {
            $rules = config("validations.{$api}.rules");
        }
        $messages = config("validations.{$api}.messages");
        if (!$messages) {
            $messages = config("validations.{$api}.messages");
        }
        if ($rules && $messages) {
            $messages = collect($messages)->map(function ($message) {
                return __($message);
            })->toArray();
            $payload = Request::only(array_keys($rules));
            $validator = Validator::make($payload, $rules, $messages);
            if ($validator->fails()) {
                throw new ValidationException($validator, Response::HTTP_BAD_REQUEST);
                $this->error($validator->errors()->first(), 400);
            }
        }
    }

    /**
     * Common Response Structure
     *
     * @param $message
     * @param $data
     * @param $status
     * @return array
     */
    public function successFailResponse($message,$data=null,$status,$meta=null)
    {
        $respData = [
            'message' => $message,
            'data' => $data,
            'code' => $status,
        ];
        if($meta != null){
            $respData['meta']=$meta;
        }
        return $respData;
    }

    public static function generateAccessToken($user)
    {
        $user = CmsUser::whereUsername($user->username)->first();
        $aouthTokenDetail = $user->createToken('Token Name');
        if ($aouthTokenDetail->accessToken) {
            $tokenDetail = [];
            $tokenDetail['token'] = $aouthTokenDetail->accessToken;
            $tokenDetail['token_id'] = $aouthTokenDetail->token->id;
            return $tokenDetail;
        }
    }
}
