<?php

namespace App\Http\Controllers;
use App\Contracts\ProductContract;
use Illuminate\Http\Request;

class ProductController extends Controller {

	public function getProductsFromFile(Request $request,ProductContract $obj){
		$file = $request->input('file_name');
		$response = $obj->getProductsFromFile($file);
		return $response;
	}
}