<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\ApiContract;
use Illuminate\Validation\ValidationException;
use App\Contracts\CustomerContract;

class CustomerController extends Controller
{
    /**
     * Create and returns the list of customers
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     */
    public function customerList(Request $request,ApiContract $data)
    {

        $response = $data->customerList($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Customer List.', $response['data'], 200,$response['meta']);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }
    public function customers(Request $request,CustomerContract $customer){
        $user_data['user_id'] = $request->input('user_id');
        // $user_data['user_id'] = 1;
        $data = $customer->customers($user_data);
        return $data;
    }
    public function getCustomer(Request $request,CustomerContract $customer){
        $data=['agent_id'=>$request->input('agent_id'),'email'=>$request->input('email_id')];
        $data = $customer->getCustomer($data);
        return $data;
    }

    /**
     * Generate the details of customer
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws ValidationException
     */
    public function viewCustomer(Request $request, ApiContract $data)
    {
        $this->validateRequest('CustomerValidation');
        $response = $data->viewCustomer($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Customer detail.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * Add and returns the details of customer
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws ValidationException
     */
    public function addCustomer(Request $request,ApiContract $data)
    {
        $this->validateRequest('AddCustomerValidation');
        $response = $data->addCustomer($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Customer Added.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * Edit and returns the details of customer
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws ValidationException
     */
    public function editCustomer(Request $request,ApiContract $data)
    {
        $this->validateRequest('EditCustomerValidation');
        $response = $data->editCustomer($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Customer Edited.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }
}
