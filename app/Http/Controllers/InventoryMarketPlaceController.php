<?php

namespace App\Http\Controllers;

use App\Contracts\ApiContract;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class InventoryMarketPlaceController extends Controller
{

    /**
     * Returns feedback of added inventory market place service details
     *
     * @param Request $request
     * @param ApiContract $obj
     * @return array
     * @throws ValidationException
     */
    public function addInventoryMarketPlace(Request $request, ApiContract $obj)
    {
        $this->validateRequest('inventoryMarketPlaceValidation');
        $response = $obj->addInventoryMarketPlace($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Inventory Market Place Added.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * Returns feedback of edited inventory market place service details
     *
     * @param Request $request
     * @param ApiContract $obj
     * @return array
     * @throws ValidationException
     */
    public function editInventoryMarketPlace(Request $request, ApiContract $obj)
    {
        $this->validateRequest('inventoryMarketPlaceValidation');
        $response = $obj->editInventoryMarketPlace($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Inventory Market Place Edited.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * Returns feedback of inventory market place data by given domain_id
     *
     * @param ApiContract $obj
     * @return array
     */
    public function viewInventoryMarketPlace(ApiContract $obj)
    {
        $response = $obj->viewInventoryMarketPlace();
        if ($response['code'] === 200) {
            return $this->successFailResponse('Inventory Market Place List.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }
}