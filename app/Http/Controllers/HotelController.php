<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Contracts\SearchContract;
use App\Contracts\HotelContract;

class HotelController extends Controller{

	public function searchHotels(Request $request,SearchContract $searchObj){
		$origin_city = $request->input('city_id');
		$checkin_date = $request->input('check_in_date');
		$default_nights = $request->input('default_nights') ?? '';

		// $origin_city = 190;
		// $checkin_date = '2019-08-12';

		$search_preference=[];
		if($request->has('accomodation_rating')){
			$search_preference['accomodation_rating']=$request->input('accomodation_rating');
		}

		$search_text = '';
        if ($request->has('search')) {
            $search_text = $request->input('search');
        }

		$response = $searchObj->searchHotels($origin_city,$checkin_date,$search_preference,$search_text,$default_nights);
		return ['code'=>200,'data'=>$response,'message'=>"Hotels"];
	}
	public function hotelDetails(Request $request,HotelContract $hotelObj){
		$response = $hotelObj->hotel_details('expedia',$request);		
		return ['code'=>200,'data'=>$response,'message'=>"Hotel Details"];
	}
}