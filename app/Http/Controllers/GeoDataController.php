<?php

namespace App\Http\Controllers;

use App\Contracts\ApiContract;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class GeoDataController extends Controller
{

    /**
     * add the geo data as per domain
     *
     * @param Request $request
     * @param ApiContract $obj
     * @return array
     * @throws ValidationException
     */
    public function addGeoData(Request $request, ApiContract $obj)
    {
        $this->validateRequest('GeoDataValidation');
        $response = $obj->addGeoData($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Geo Data Added.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * edit the geo data as per domain
     *
     * @param Request $request
     * @param ApiContract $obj
     * @return array
     * @throws ValidationException
     */
    public function editGeoData(Request $request, ApiContract $obj)
    {
        $this->validateRequest('GeoDataValidation');
        $response = $obj->editGeoData($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Geo Data Edited.', $response['data'], 200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * Returns the details of geo data as per domain_id
     *
     * @param Request $request
     * @param ApiContract $obj
     * @return array
     * @throws ValidationException
     */
    public function viewGeoData(Request $request, ApiContract $obj)
    {
        $this->validateRequest('GeoDataValidation');
        $response = $obj->viewGeoData($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Geo Data List.',$response['data'],200);
        } else {
            $message = isset($response['message']) ? $response['message'] : 'Bad Request.';
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

}