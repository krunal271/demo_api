<?php

namespace App\Http\Controllers;


use App\Contracts\ApiContract;
use App\Contracts\UserContract;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class GeneralController extends Controller
{

    public function checkLogin(UserContract $userObj)
    {
        $credentials = [
            'username' => "test3@gmail.com",
            'password' => "1234567890",
        ];
        return $user = $userObj->login($credentials);
    }

    /**
     * For add licensee detail
     *
     * @param ApiContract $apiObj
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    public function addLicenseeDetail(ApiContract $apiObj, Request $request)
    {
        $this->validateRequest('addLicenseeValidation');
        $requestParams=$request->all();
        $response = $apiObj->addLicenseeDetail($requestParams);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Licensee Detail Added.', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"] : "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message,null,$code);
        }
    }

    /**
     * For edit licensee detail
     *
     * @param ApiContract $apiObj
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    public function editLicenseeDetail(ApiContract $apiObj, Request $request)
    {
        $this->validateRequest('editLicenseeValidation');
        $requestParams=$request->all();
        $response = $apiObj->editLicenseeDetail($requestParams);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Licensee Detail Edited.', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"] : "Bad Request";
            $code = isset($response["code"]) ? $response["code"] : 400;
            return $this->successFailResponse($message,null,$code);
        }
    }

    /**
     * Returns list of users
     *
     * @param ApiContract $apiObj
     * @return array
     */
    public function licenseeList(ApiContract $apiObj)
    {
        $response = $apiObj->licenseeList();
        if ($response['code'] === 200) {
            return $this->successFailResponse('Licensee List.', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"] : "Bad Request";
            $code = isset($response["code"]) ? $response["code"] : 400;
            return $this->successFailResponse($message,null,$code);
        }
    }

    /**
     * Returns detail of licensee
     *
     * @param ApiContract $apiObj
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    public function licenseeDetail(ApiContract $apiObj, Request $request)
    {
        $this->validateRequest('licenseeDetailValidation');
        $response = $apiObj->licenseeDetail($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Licensee Detail.', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"] : "Bad Request";
            $code = isset($response["code"]) ? $response["code"] : 400;
            return $this->successFailResponse($message,null,$code);
        }
    }

    /**
     * Returns all per set account options
     *
     * @param ApiContract $apiObj
     * @return array
     */
    public function preSetAccountOptionList(ApiContract $apiObj)
    {
        $response = $apiObj->getAllPreSetAccountOptions();
        if ($response['code'] === 200) {
            return $this->successFailResponse('Pre set account Options', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"] : "Bad Request";
            $code = isset($response["code"]) ? $response["code"] : 400;
            return $this->successFailResponse($message,null,$code);
        }
    }
}