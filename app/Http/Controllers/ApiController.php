<?php

namespace App\Http\Controllers;

use App\Models\CmsUser;
use Illuminate\Http\Request;
use App\Contracts\ApiContract;
use App\Contracts\CacheContract;
use App\Contracts\SearchContract;

class ApiController extends Controller
{

    public function currencies(ApiContract $apiObj)
    {
        $response = $apiObj->currencies();

        return ['code' => 200, 'data' => $response['data'], 'message' => 'Currencies List'];
    }

    public function prefences(ApiContract $apiObj)
    {
        $response = $apiObj->preference();

        return ['code' => 200, 'data' => $response, 'message' => "Preference List"];
    }

    public function cities(CacheContract $cacheObj)
    {
        $cities = $cacheObj->getAllCities();
        return ['code' => 200, 'data' => $cities, 'message' => "Cities"];
    }

    public function autoRoutes(Request $request, ApiContract $apiObj)
    {
        $data['origin_city'] = $request->input('origin_city')['value'];
        // $data['origin_city'] = 225;
        $routes = $apiObj->autoRoutes($data['origin_city']);
        return ['code' => 200, 'data' => $routes, 'message' => "Cities"];
    }

    public function cityCache(CacheContract $cacheObj)
    {
        $routes = $cacheObj->cacheRoutes();

        return ['code' => 200, 'data' => $routes, 'message' => "Cities"];
    }

    public function autoRoutesGet(ApiContracts $apiObj)
    {
        $data['origin_city'] = 225;
        $routes = $apiObj->autoRoutes($data['origin_city']);
        // dd($routes);
        return ['code' => 200, 'data' => $routes, 'message' => "Cities"];
    }

    public function prefetchActivity(Request $request, SearchContract $searchObj)
    {
        $origin_city = $request->input('origin_city')['value'] ? $request->input('origin_city')['value'] : $request->input('city_id');
        $checkin_date = $request->input('check_in_date');
        $search_preference = [];
        $pax_info = [];
        $pax_info['total_adult'] = 2;
        $pax_info['total_child'] = 2;
        $pax_info['child_age'][0] = 7;
        $pax_info['child_age'][1] = 12;

        if ($request->has('activity_category_id')) {
            $search_preference['activity_category_id'] = $request->input('activity_category_id');
        }

        $response = $searchObj->searchActivities($origin_city, $checkin_date, $search_preference, $pax_info);
        return ['code' => 200, 'data' => $response, 'message' => "Activity"];
    }

    public function searchHotels(Request $request, SearchContract $searchObj)
    {
        $origin_city = $request->input('city_id');
        $checkin_date = $request->input('check_in_date');
        $search_preference = [];
        if ($request->has('accomodation_rating')) {
            $search_preference['accomodation_rating'] = $request->input('accomodation_rating');
        }

        $search_text = '';
        if ($request->has('search')) {
            $search_text = $request->input('search');
        }        
        
        $response = $searchObj->searchHotels($origin_city, $checkin_date, $search_preference,$search_text);
        return ['code' => 200, 'data' => $response, 'message' => "Hotels"];
    }


    public function searchTransport(Request $request, SearchContract $searchObj)
    {
        $origin_city = $request->input('city_id');
        $destination_city = $request->input('destination_city');
        $checkin_date = $request->input('check_in_date');

        $response = $searchObj->searchTransport($origin_city, $destination_city, $checkin_date);
        return ['code' => 200, 'data' => $response, 'message' => "Transport"];
    }

    public function mapData(ApiContract $apiObj, Request $request)
    {
        $origin_city = $request->input('source_city');
        $destination_city = $request->input('destination_city');
        $response = $apiObj->mapData($origin_city, $destination_city);

        return ['code' => 200, 'data' => $response, 'message' => "Map Data"];
    }

    public function getAllCities(ApiContract $apiObj)
    {
        $response = $apiObj->getAllCities();
        return ['code' => 200, 'data' => $response, 'message' => "All Cities with Lat Long"];
    }

    public function autoRouteGet(ApiRepository $apiRepository)
    {
        $data['origin_city'] = 225;
        $routes = $apiRepository->autoRoutes($data['origin_city']);

		return ['code'=>200,'data'=>$routes,'message'=>"Cities"];
	}
    public function findRoutes(ApiContract $apiObj,Request $request){
        $input = $request->all();
        $routes = $apiObj->findRoutes($input['source_city_id'], $input['destination_city_id']);
        return ['code' => 200, 'data' => $routes, 'message' => "Routes"];
    }
	public function cityInfo(Request $request,CacheContract $cacheObj){
		$input = $request->all();
		// $origin_city = $input[0]['city_id'];
		// $destination_city = $input[1]['city_id'];
        $origin_city = $input['origin_city_id'];
        $destination_city = $input['destination_city_id'];
		$response['origin_city'] = $cacheObj->getRouteCityById($origin_city);
		$response['destination_city'] = $cacheObj->getRouteCityById($destination_city);
		return ['code'=>200,'data'=>$response,'message'=>"City Info"];
	}
	public function nearestCities(Request $request,ApiContract $apiObj){
		$center_latitude = $request->input('lat');
		$center_logitude = $request->input('lng');
		$distance = $request->input('distance');
		$zoom_level = $request->input('zoom_level');

		$response = $apiObj->findNearestCities($center_latitude,$center_logitude,$distance,$zoom_level);
		return ['code'=>200,'data'=>$response,'message'=>"City Info"];
	}

    /**
     * Returns timezone list
     *
     * @param CacheContract $cacheObj
     * @return array
     */
    public function timezones(CacheContract $cacheObj)
    {
        $response = $cacheObj->getAllTimezones();
        return ['code' => 200, 'data' => $response, 'message' => 'Timezones List'];
    }

    /**
     * Returns cities list
     *
     * @param CacheContract $cacheObj
     * @return array
     */
    public function countries(CacheContract $cacheObj)
    {
        $response = $cacheObj->getAllCountries();
        return ['code' => 200, 'data' => $response, 'message' => 'Countries List'];
    }

	public function getProducts(ApiContract $apiObj) {
        $response = $apiObj->getProducts();
        return ['code'=>200,'data'=>$response,'message'=>"Products"];
    }
    public function getCityById(Request $request,CacheContract $cacheObj){
        $input = $request->input();
        $origin_city = $input['id'];
        $response = $cacheObj->getRouteCityById($origin_city);
        return ['code'=>200,'data'=>$response,'message'=>"Cities"];
    }

}