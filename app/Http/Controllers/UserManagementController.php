<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\ApiContract;

class UserManagementController extends Controller {

    /**
     * Return list of cms roles
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     */
    public function getAllRoles(Request $request, ApiContract $data)
    {
        $response = $data->getAllRoles();
        if ($response['code'] === 200) {
            return ['code' => 200, 'data' => $response['data'], 'message' => "User Management Roles"];
        } else {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * add cms user
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addCmsUser(Request $request, ApiContract $data)
    {
        $this->validateRequest('addCmsUser');
        $response = $data->addCmsUser($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('User added successfully', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * edit cms user
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function editCmsUser(Request $request, ApiContract $data)
    {
        $this->validateRequest('editCmsUser');
        $response = $data->editCmsUser($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('User updated successfully', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * cms user list
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function cmsUserList(Request $request, ApiContract $data)
    {
        $this->validateRequest('cmsUserList');
        $response = $data->cmsUserList($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Cms user list', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * role-wise permission list
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     */
    public function rolesPermissionsList(Request $request, ApiContract $data)
    {
        $response = $data->rolesPermissionsList($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Roles and permissions', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * permissions list
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     */
    public function permissionsList(Request $request, ApiContract $data)
    {
        $response = $data->permissionsList($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('Permissions', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }

    /**
     * CMS User Details
     *
     * @param Request $request
     * @param ApiContract $data
     * @return array
     */
    public function cmsUserDetail(Request $request,ApiContract $data)
    {
        $response = $data->cmsUserDetail($request);
        if ($response['code'] === 200) {
            return $this->successFailResponse('User Details', $response['data'], 200);
        } else {
            $message = isset($response["message"]) ? $response["message"]: "Bad Request";
            $code = isset($response['code']) ? $response['code'] : 400;
            return $this->successFailResponse($message, null, $code);
        }
    }
}