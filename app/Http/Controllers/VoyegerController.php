<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\SearchContract;
use App\Contracts\HotelContract;

class VoyegerController extends Controller {

	public function searchHotels(Request $request,SearchContract $searchObj){
		$origin_city = $request->input('city_id');
		$checkin_date = $request->input('check_in_date');
		$default_nights = $request->input('default_nights') ?? '';

		$search_preference=[];
		if($request->has('accomodation_rating')){
			$search_preference['accomodation_rating']=$request->input('accomodation_rating');
		}

		$search_text = '';
        if ($request->has('search')) {
            $search_text = $request->input('search');
        }

		$response = $searchObj->searchVoyegerHotels($origin_city,$checkin_date,$search_preference,$search_text,$default_nights);
		if($response) {
			return ['code'=>200,'data'=>$response,'message'=>"Hotels"];
		} else {
			return ['code'=>300,'message'=>"Hotels not found"];
		}
		
	}

	// public function voyegerHotelDetail(Request $request,SearchContract $searchObj)
	// {
	// 	$hotel_id = $request->input('hotel_id');
	// 	$checkin_date = $request->input('check_in_date');
	// 	$default_nights = $request->input('default_nights') ?? '';
	// 	$response = $searchObj->searchVoyegerHotels($origin_city,$checkin_date,$search_preference,$search_text,$default_nights);
	// 	return ['code'=>200,'data'=>$response,'message'=>"Hotel Detail"];
	// }

	public function voyegerHotelDetail(Request $request,HotelContract $hotelObj){
		$hotel_id = $request->input('hotel_id');
		$check_in_date = $request->input('check_in_date');
		$default_nights = $request->input('default_nights') ?? '';
		$response = $hotelObj->voyeger_hotel_details($hotel_id,$check_in_date,$default_nights);		
		return ['code'=>200,'data'=>$response,'message'=>"Hotel Details"];
	}
}