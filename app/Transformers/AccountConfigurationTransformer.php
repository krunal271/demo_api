<?php


namespace App\Transformers;

use App\Models\GeoData;
use App\Models\GrossNetConfigurations;
use App\Models\MailConfigurations;
use App\Models\PayByUserConfiguration;
use App\Models\PaymentConfigurationMapping;
use App\Models\Status;
use League\Fractal\TransformerAbstract;

class AccountConfigurationTransformer extends TransformerAbstract
{
    /**
     * Set transformer and returns the response
     *
     * @param GeoData $response
     * @return array
     */
    public function transform($response)
    {
        $licenseeId = $response['licensee_id'];
        $paymentTerms = PaymentConfigurationMapping::whereLicenseeId($licenseeId)->whereStatus(Status::$ACTIVE)->with('paymentTerms')->get();
        $paymentTermsData = [];
        foreach ($paymentTerms as $val) {
            $subPaymentTerms['id'] = $val->paymentTerms['id'];
            $subPaymentTerms['payment_terms'] = $val->paymentTerms['payment_terms'];
            $subPaymentTerms['status'] = isset($val->paymentTerms->selectedTerm['pt_id']) ? true : false;
            array_push($paymentTermsData, $subPaymentTerms);
        }
        $grossConfig = GrossNetConfigurations::whereLicenseeId($licenseeId)->whereStatus(Status::$ACTIVE)->get();
        $grossConfigData = [];
        foreach ($grossConfig as $value) {
            $grossConfigData[$value['name']] = [
                'rate_type' => $value['rate_type'],
                'commission' => $value['commission'],
                'agent_enterprice' => $value['agent_enterprice'],
                'agent_wholesale' => $value['agent_wholesale']
            ];
        }
        $payByUSerConfiguration = PayByUserConfiguration::whereLicenseeId($licenseeId)->select('amount_per_user',
            'payment_frequency', 'payment_provider', 'client_custom_payment', 'manual_payment', 'taxes',
            'edit_order_id_format', 'consumer_accounts', 'payment_configuration_language')->first();
        $mailConfig = MailConfigurations::whereLicenseeId($licenseeId)->whereStatus(Status::$ACTIVE)->select('mail_host',
            'mail_port', 'mail_username', 'mail_password', 'mail_from_address', 'mail_from_name',
            'mail_encryption')->first();
        $response['payment_terms'] = $paymentTermsData;
        $response['gross_net_configuration'] = $grossConfigData;
        $response['pay_by_user_configuration'] = $payByUSerConfiguration;
        $response['mail_configuration'] = $mailConfig;

        return [
            'licensee_id' => isset($response['licensee_id']) ? (int)$response['licensee_id'] : '',
            'payment_terms' => isset($paymentTermsData) ? $paymentTermsData :'',
            'gross_net_configuration' => isset($grossConfigData)? $grossConfigData :'',
            'pay_by_user_configuration' => isset($payByUSerConfiguration) ? $payByUSerConfiguration :'',
            'mail_configuration' => isset($mailConfig) ? $mailConfig :''
        ];
    }
}