<?php


namespace App\Transformers;

use App\Models\CmsUser;
use App\Models\Licensees;
use League\Fractal\TransformerAbstract;

class AuthTransformer extends TransformerAbstract
{
    /**
     * Set transformer and returns the response
     *
     * @param Licensees $response
     * @return array
     */
    public function transform(CmsUser $response)
    {
        return [
            'id' => isset($response->id) ? (int)$response->id : '',
            'domain_id' => isset($response->domain_id) ? (int)$response->domain_id : '',
            'username' => isset($response->username) ? $response->username : '',
            'token_data' => isset($response->token) && $response->token!= null ? $response->token: '',
            'role' => isset($response->role_name) && $response->role_name!= null ? $response->role_name: '',
        ];
    }
}
