<?php


namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\CmsUser;

class CmsUserTransformer extends TransformerAbstract
{
    /**
     * Set transformer and returns the response
     *
     * @param CmsUser $response
     * @return array
     */
    public function transform(CmsUser $response)
    {
        $userDetails = $response->userDetails;
        return [
            'user_id' => isset($response->id) ? (int)$response->id : 0,
            'username' => isset($response->username) ? $response->username : '',
            'role_id' => isset($response->role_id) ? $response->role_id : '',
            'role' => isset($response->role_name) ? $response->role_name : '',
            'first_name' => isset($userDetails->first_name) ? $userDetails->first_name : '',
            'last_name' => isset($userDetails->last_name) ? $userDetails->last_name : '',
            'email' => isset($userDetails->email) ? $userDetails->email : '',
            'contact_number' => isset($userDetails->contact_number) ? $userDetails->contact_number : '',
            'user_type' => isset($userDetails->user_type) ? $userDetails->user_type : '',
            'user_type_name' => isset($userDetails->user_type_name) ? $userDetails->user_type_name : '',
        ];
    }
}