<?php

namespace App\Transformers;

use App\Models\Tasks;
use League\Fractal\TransformerAbstract;

class TaskTransformer extends TransformerAbstract
{
    /**
     * Set transformer and returns the response
     *
     * @param Licensees $response
     * @return array
     */
    public function transform(Tasks $response)
    {
        return [
            'assigned_id' => isset($response->assigned_id) ? $response->assigned_id : '',
            'due_date' => isset($response->assigned_id) ? $response->assigned_id : '',
            'status' => isset($response->assigned_id) ? $response->assigned_id : '',
            'consultant_name' => isset($response->assigned_id) ? $response->assigned_id : '',
            'task' => isset($response->assigned_id) ? $response->assigned_id : '',
            'title' => isset($response->assigned_id) ? $response->assigned_id : '',
            'snooze' => isset($response->assigned_id) ? $response->assigned_id : ''
        ];
    }
}