<?php
namespace App\Transformers;
use League\Fractal\TransformerAbstract;
use App\Models\City;

class CityTransformer extends TransformerAbstract
{
	public function transform(City $city){
		return [
			'city_id'=>(int) $city->id,
			'city_name'=>$city->name,
			'country_code'=>$city->country->code ?? '',
			'country_id'=>$city->country->id ?? '',
			'country_name'=>$city->country->name ?? '',
			'default_nights'=>$city->default_nights ?? '',
			'description'=>$city->description ?? '',
			'lat'=>$city->latlong->lat ?? '',
			'lng'=>$city->latlong->lng ?? '',
			'small_image'=>$city->small_image ?? ''
		];
	}
}