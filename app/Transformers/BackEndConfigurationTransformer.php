<?php

namespace App\Transformers;

use App\Models\Licensees;
use League\Fractal\TransformerAbstract;

class BackEndConfigurationTransformer extends TransformerAbstract
{
    /**
     * Set transformer and returns the response
     *
     * @param Front-End Configuration $response
     * @return array
     */
    public function transform($response)
    {
        $backendConfiguration = [];
        foreach ($response->backendConfiguration()->get() as $key => $backConfig) {
            $backendConfiguration[$key]["backend_services_id"] = $backConfig->backend_services_id;
            $backendConfiguration[$key]["name"] = $backConfig->name;
        }
        return [
            'domain_id' => isset($response->id) ? (int)$response->id : '',
            'domain_name' => isset($response->domain_name) ? $response->domain_name : '',
            'backendConfigurations' => $backendConfiguration
        ];

    }
}