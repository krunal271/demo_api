<?php

namespace App\Transformers;

use App\Models\Customer;
use League\Fractal\TransformerAbstract;

class CustomerTransformer extends TransformerAbstract
{
    /**
     * Set transformer and returns the response
     *
     * @param Customer $response
     * @return array
     */
    public function transform(Customer $response)
    {
        return [
            'id' => isset($response->id) ? (int)$response->id : '',
            'customer_id' => isset($response->customer_id) ? $response->customer_id : '',
            'title' => isset($response->title) ? $response->title : '',
            'first_name' => isset($response->first_name) ? $response->first_name : '',
            'last_name' => isset($response->last_name) ? $response->last_name : '',
            'gender' => isset($response->gender) ? $response->gender : '',
            'country_code' => isset($response->country_code) ? $response->country_code : '',
            'phone' => isset($response->phone) ? $response->phone : '',
            'email' => isset($response->email) ? $response->email : '',
            'contact_method' => isset($response->contact_method) ? $response->contact_method : '',
            'domain' => isset($response->domain) ? $response->domain : '',
            'frequently_used_product' => isset($response->frequently_used_product) ? $response->frequently_used_product : '',
            'accommodation_type' => isset($response->accommodation_type) ? $response->accommodation_type : '',
            'room_type' => isset($response->room_type) ? $response->room_type : '',
            'transport_type' => isset($response->transport_type) ? $response->transport_type : '',
            'cabin_class_type' => isset($response->cabin_class_type) ? $response->cabin_class_type : '',
            'seat_type' => isset($response->seat_type) ? $response->seat_type : '',
            'meal_type' => isset($response->meal_type) ? $response->meal_type : '',
            'interests' => isset($response->interests) ? $response->interests : '',
            'conversation_notes' => isset($response->conversation_notes) ? $response->conversation_notes : '',
            'middle_name' => isset($response->middle_name) ? $response->middle_name : '',
            'nationality' => isset($response->nationality) ? $response->nationality : '',
            'birth_date' => isset($response->birth_date) ? $response->birth_date : '',
            'passport_number' => isset($response->passport_number) ? $response->passport_number : '',
            'passport_expiry_date' => isset($response->passport_expiry_date) ? $response->passport_expiry_date : '',
            'passport_issue_date' => isset($response->passport_issue_date) ? $response->passport_issue_date : '',
            'address_1' => isset($response->address_1) ? $response->address_1 : '',
            'address_2' => isset($response->address_2) ? $response->address_2 : '',
            'city' => isset($response->city) ? $response->city : '',
            'state' => isset($response->state) ? $response->state : '',
            'country' => isset($response->country) ? $response->country : '',
            'postal_code' => isset($response->postal_code) ? $response->postal_code : '',
            'preferred_language' => isset($response->preferred_language) ? $response->preferred_language : '',
            'additional_info' => isset($response->additional_info) ? $response->additional_info : '',
        ];
    }
}