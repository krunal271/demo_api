<?php

namespace App\Listeners;

use App\Events\SaveToRedisCache;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cache as Cache;

class SaveToRedisCacheListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SaveToRedisCache $event
     * @return void
     */
    public function handle(SaveToRedisCache $event)
    {
        $key  = $event->key;
        $data = $event->data;
        $expiresAt = \Carbon\Carbon::now()->addWeeks(50);

        Cache::put($key, $data, $expiresAt);
    }
}
