<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Templates extends Model
{
    use SoftDeletes;
	protected $table = 'templates';
    protected $primaryKey = 'id';
    public $timestamps = true;
}