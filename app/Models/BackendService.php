<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BackendService extends Model
{
    use SoftDeletes;
	
    protected $table = 'backend_services';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $hidden = ['deleted_at'];

    public function parent() {
        return $this->belongsToOne(static::class, 'parent_id');
    }

    public function children() {
        return $this->hasMany(static::class, 'parent_id')->select('id','parent_id','name')->orderBy('name', 'asc');
    }

}