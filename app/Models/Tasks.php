<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
class Tasks extends Model {
	protected $table = 'tasks';
    protected $guarded = array('id');

    const ACTIVE=1, INACTIVE=2;
    const ACTION=1, SNOOZE=2, COMPLETE=3;
    const TASK_STATUS_COMPLETE=1,TASK_STATUS_INCOMPLETE=0;
	public function setDueDateAttribute($value)
    {
        $this->attributes['due_date'] = Carbon::parse($value)->toDateString();
    }
    public function scopeActive($q){
	    return $q->where('status',self::ACTIVE);
    }

    public function scopeAction($q){
	    return $q->wheredueDate(carbon::now()->toDateString())->whereTaskStatus(self::TASK_STATUS_INCOMPLETE);
    }

    public function scopeSnooze($q){
        return $q->where('due_date','>',carbon::now()->toDateString())->whereTaskStatus(self::TASK_STATUS_INCOMPLETE);
    }

    public function scopeComplete($q){
	    return $q->whereTaskStatus(self::TASK_STATUS_COMPLETE);
    }
}
?>