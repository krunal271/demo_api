<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountNotes extends Model
{
    use SoftDeletes;
	protected $table = 'account_notes';
    protected $primaryKey = 'id';
    public $timestamps = true;
}