<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class CmsUser extends Model implements AuthenticatableContract, AuthorizableContract
{
    use SoftDeletes, HasRoles, Authenticatable, Authorizable, HasApiTokens;

    protected $table = 'cms_user';
    public $timestamps = true;

    protected $hidden = ['deleted_at'];
    protected $guard_name = 'api';

    public function getRoleNameAttribute()
    {
        $role = $this->roles()->first();
        $roleName = $role ? $role->name : "";
        return $roleName;
    }

    public function getRoleIdAttribute()
    {
        $role = $this->roles()->first();
        $roleId = $role ? $role->id : "";
        return $roleId;
    }

    public function domains()
    {
        return $this->hasMany('App\Models\Domain', 'id', 'domain_id');
    }

    public function userDetails()
    {
        return $this->hasOne('App\Models\UserDetail', 'user_id', 'id');
    }
}
