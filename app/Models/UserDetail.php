<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserDetail extends Model
{
    protected $table = 'user_detail';
    public $timestamps = true;
    protected $hidden = ['id','deleted_at', 'created_at', 'updated_at','status'];

    public static $USERTYPES = [ "1" => "customer" , "2" => "cms_admin" ];
    public static $CUSTOMER = "1";
    public static $CMSADMIN = "2";

    public function getUserTypeNameAttribute() {
        $userTypeValue = "";

        if (array_key_exists($this->user_type,self::$USERTYPES)) {
            $userTypeValue = self::$USERTYPES[$this->user_type];
        }
        return $userTypeValue;
    }
}
