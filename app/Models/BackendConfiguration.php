<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BackendConfiguration extends Model
{
    use SoftDeletes;

    protected $table = 'backend_configurations';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $hidden = ['deleted_at'];

    public function domains() {
        return $this->belongsTo('App\Models\Domain','domain_id','id');
    }

    public function getNameAttribute() {
        $name = '';
        $service = BackendService::select('name')->whereId($this->backend_services_id)->first();
        if($service) {
            $name = $service->name;
        }
        return $name;
    }

}