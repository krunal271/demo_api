<?php

namespace App\Events;

class SaveToRedisCache extends Event
{
    public $key;
    public $data;

    /**
     * Create a new event instance.
     *
     * SaveToRedisCache constructor.
     * @param $key
     * @param $data
     */
    public function __construct($key, $data)
    {
        $this->key = $key;
        $this->data = $data;
    }
}
