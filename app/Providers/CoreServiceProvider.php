<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Contracts\ApiContract','\App\Core\Service\ApiService');
        $this->app->bind('App\Contracts\TaskContract','\App\Core\Service\TaskService');
        $this->app->bind('App\Contracts\SearchContract','\App\Core\Service\SearchService');
        $this->app->bind('App\Contracts\CacheContract','\App\Core\Service\CacheService');
        $this->app->bind('App\Contracts\FeedContract','\App\Core\Service\FeedService');
        $this->app->bind('App\Contracts\UserContract','\App\Core\Service\UserService');
        $this->app->bind('App\Contracts\HotelContract','\App\Core\Service\HotelService');
        $this->app->bind('App\Contracts\TourContract','\App\Core\Service\TourService');
        $this->app->bind('App\Contracts\ActivityContract','\App\Core\Service\ActivityService');
        $this->app->bind('App\Contracts\CustomerContract','\App\Core\Service\CustomerService');
        $this->app->bind('App\Contracts\ItineraryContract','\App\Core\Service\ItineraryService');
        $this->app->bind('App\Contracts\TransportContract','\App\Core\Service\TransportService');
        $this->app->bind('App\Contracts\ProductContract','\App\Core\Service\ProductService');
    }
}

