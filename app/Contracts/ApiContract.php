<?php

namespace App\Contracts;

interface ApiContract
{

	public function currencies();
	public function preference();
	public function autoRoutes($origin_city);
	public function mapData($origin_city,$destination_city);
	public function getAllCities();
	public function findNearestCities($center_latitude,$center_logitude,$distance,$zoom_level);
	public function findRoutes($origin_city,$destination_city);
    public function addLicenseeDetail($request);
    public function editLicenseeDetail($request);
    public function licenseeList();
    public function licenseeDetail($request);
    public function getAllPreSetAccountOptions();
    public function getProducts();
    public function addFrontendConfiguration($request);
    public function editFrontendConfiguration($request);
    public function frontendConfigurationDetail($request);
    public function backendServices();
    public function addBackendConfiguration($request);
    public function editBackendConfiguration($request);
    public function backendConfigurationDetail($request);
    public function addGeoData($request);
    public function editGeoData($request);
    public function viewGeoData($request);
    public function addInventoryMarketPlace($request);
    public function editInventoryMarketPlace($request);
    public function viewInventoryMarketPlace();
	public function getAllRoles();
    public function addCmsUser($request);
    public function editCmsUser($request);
    public function viewAccountConfiguration($request);
    public function addAccountConfiguration($request);
    public function editAccountConfiguration($request);
    public function addAccountNotes($request);
    public function editAccountNotes($request);
    public function salesRepresentativeList();
    public function loginCmsUser($request);
    public function cmsUserList($request);
    public function rolesPermissionsList($request);
    public function permissionsList($request);
    public function cmsUserDetail($request);

}