<?php

namespace App\Contracts;

interface ActivityContract
{
	public function getViatorActivityDetail($request);
	public function getViatorHotelList($request);
	public function listActivities($file);
}