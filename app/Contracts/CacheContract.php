<?php

namespace App\Contracts;

interface CacheContract
{
    public function getAllCities();

    public function cacheRoutes();

    public function getCityById($id);
    public function getRouteCityById($id);

    public function getAllTimezones();

    public function getAllCountries();
}