<?php

namespace App\Contracts;

interface TaskContract
{
    public function getTaskList($request);
    public function addTaskData($request);
    public function editTaskStatus($request);
    public function addToComplete($request);
    public function deleteTask($request);
    public function addtoSnooze($request);
}
