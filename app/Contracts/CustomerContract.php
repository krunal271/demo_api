<?php

namespace App\Contracts;
interface CustomerContract{
	
	public function customers($request);
	public function getCustomer($user_data);
}