<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class FindRouteDetailType extends GraphQLType
{
    protected $attributes = [
        'name' => 'findRouteDetails',
        'description' => 'A type City Lat Long',
    ];


    public function type()
    {
        return GraphQL::type('findRouteDetails');
    }

    /**
     * Response fields
     *
     * @return array
     */
    public function fields()
    {
        return [
            'city_id' => [
                'type' => Type::int(),
                'description' => 'The id of the city'
            ],
            'city_name' => [
                'type' => Type::string(),
                'description' => 'The name of the city'
            ],
            'country_id' => [
                'type' => Type::int(),
                'description' => 'The country id of the city'
            ],
            'country_name' => [
                'type' => Type::string(),
                'description' => 'The country name of the city'
            ],
            'country_code' => [
                'type' => Type::string(),
                'description' => 'The country code of the city'
            ],
            'lat' => [
                'type' => Type::string(),
                'description' => 'The latitude for city'
            ],
            'lng' => [
                'type' => Type::string(),
                'description' => 'The longitude of the city'
            ],
            'default_nights' => [
                'type' => Type::string(),
                'description' => 'The default nights of the city'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of the city'
            ],
            'small_image' => [
                'type' => Type::string(),
                'description' => 'The small image of the city'
            ]

        ];
    }
}
