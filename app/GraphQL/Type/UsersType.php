<?php

namespace App\GraphQL\Type;

use App\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UsersType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Users',
        'description' => 'A type',
        //'model' => User::class,
    ];

    /**
     * Response field of users
     * @return array
     */

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the user',
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of the user',
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'The title of the user'
            ],
            'username' => [
                'type' => Type::string(),
                'description' => 'The username of the user'
            ],
            'contact_no' => [
                'type' => Type::string(),
                'description' => 'The contact_no of the user'
            ],
            'type' => [
                'type' => Type::string(),
                'description' => 'The type of the user'
            ],
        ];
    }

    protected function resolveUSERNameField($root, $args)
    {
        return strtolower($root->username);
    }
}
