<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MapDataType extends GraphQLType
{
    protected $attributes = [
        'name' => 'mapDataRecord',
        'description' => 'A type of Map data',
    ];

    /**
     * Response fields
     *
     * @return array
     */
    public function fields()
    {
        return [
            'city_lat_long' => [
                'type' => Type::listOf(GraphQL::type('cityLatLong')),
                'description' => 'Object of city lat-long'
            ],
            'mapData' => [
                'type' => GraphQL::type('cityRouteData'),
                'description' => 'Object of map data'
            ],
        ];
    }
}
