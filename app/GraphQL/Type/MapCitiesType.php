<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MapCitiesType extends GraphQLType
{
    protected $attributes = [
        'name' => 'mapCities',
        'description' => 'A type City Lat Long',
    ];


    public function type()
    {
        return GraphQL::type('mapCities');
    }

    /**
     * Response fields
     *
     * @return array
     */
    public function fields()
    {
        return [
            'cities' => [
                'type' => Type::listOf(GraphQL::type('cityLatLong')),
                'description' => 'List of city'
            ],
        ];
    }
}
