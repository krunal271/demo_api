<?php

namespace App\GraphQL\Type;

use App\CitiesLatLong;
use App\City;
use App\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CityLatLongType extends GraphQLType
{
    protected $attributes = [
        'name' => 'CityLatLong',
        'description' => 'A type City Lat Long',
        'model' => CitiesLatLong::class,
    ];

    /**
     * response fields of city lat-long
     * @return array
     */
    public function fields()
    {
        return [
            'city_id' => [
                'type' => Type::int(),
                'description' => 'The id of the city'
            ],
            'lat' => [
                'type' => Type::string(),
                'description' => 'The latitude of the city'
            ],
            'lng' => [
                'type' => Type::string(),
                'description' => 'The longitude of the city'
            ]
        ];
    }
}
