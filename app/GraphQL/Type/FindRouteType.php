<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class FindRouteType extends GraphQLType
{
    protected $attributes = [
        'name' => 'findRoutesRecord',
        'description' => 'A type of Route data',
    ];

    /**
     * Response fields
     *
     * @return array
     */
    public function fields()
    {
        return [
            'from_city_id' => [
                'type' => Type::int(),
                'description' => 'Id of origin city'
            ],
            'from_city_name' => [
                'type' => Type::string(),
                'description' => 'Name of origin city'
            ],
            'to_city_id' => [
                'type' => Type::int(),
                'description' => 'Id of destination city'
            ],
            'to_city_name' => [
                'type' => Type::string(),
                'description' => 'Name of destination city'
            ],
            'routes' => [
                'type' => Type::listOf(Type::listOf(GraphQL::type('findRouteDetails'))),
                'description' => 'List of route details'
            ],
        ];
    }
}
