<?php

namespace App\GraphQL\Type;

use App\City;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CityType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Cities',
        'description' => 'A type',
        'model' => City::class,
    ];

    /**
     * Response field of city
     * @return array
     */
    public function fields()
    {
        return [
            'code' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the city'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of the city'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of the city',
                'privacy'       => function(array $args)
                {
                    return 1 == 1;  /* if condition true then only this field display */
                },
            ],
            'latlong' => [
                'type' => Type::listOf(GraphQL::type('latlong')),
                'description' => 'The lat-long of the city',
                'resolve' => function ($data, $args) {
                    return $data->latlong()->get();
                }
            ]
        ];
    }
}
