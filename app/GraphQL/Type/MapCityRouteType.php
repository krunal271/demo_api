<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MapCityRouteType extends GraphQLType
{
    protected $attributes = [
        'name' => 'cityRouteData',
        'description' => 'A City Route',
    ];


    public function type()
    {
        return GraphQL::type('cityRouteData');
    }

    /**
     *
     * response fields of city lat-long
     * @return array
     */
    public function fields()
    {
        return [
            'type' => [
                'type' => Type::string(),
                'description' => 'The type of the city route'
            ],
            'routes' => [
                'type' => GraphQL::type('mapCities'),
                'description' => 'Object of routes'
            ],
        ];
    }
}
