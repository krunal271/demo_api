<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MapCityLatLongType extends GraphQLType
{
    protected $attributes = [
        'name' => 'LatLong',
        'description' => 'A type City Lat Long',
    ];


    public function type()
    {
        return GraphQL::type('cityLatLong');
    }

    /**
     * Response fields
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::int(),
                'description' => 'The id of the city'
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of the city'
            ],
            'region_id' => [
                'type' => Type::int(),
                'description' => 'The region id of the city'
            ],
            'geohash' => [
                'type' => Type::string(),
                'description' => 'The geohash of the city'
            ],
            'optional_city' => [
                'type' => Type::string(),
                'description' => 'The optional id of the city'
            ],
            'default_nights' => [
                'type' => Type::string(),
                'description' => 'The default night for city'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of the city'
            ],
            'small_image' => [
                'type' => Type::string(),
                'description' => 'The small image of the city'
            ],
            'row_id' => [
                'type' => Type::int(),
                'description' => 'The row id of the city'
            ],
            'country_id' => [
                'type' => Type::string(),
                'description' => 'The country id of the city'
            ],

            'is_disabled' => [
                'type' => Type::int(),
                'description' => 'The is_disabled for city'
            ],

            'airport_codes' => [
                'type' => Type::string(),
                'description' => 'The airpot codes of the city'
            ],

            'timezone_id' => [
                'type' => Type::int(),
                'description' => 'The timezone id of the city'
            ],

            'label' => [
                'type' => Type::string(),
                'description' => 'The label of the city'
            ],

            'latlong' => [
                'type' => Type::string(),
                'description' => 'The latlong of the city'
            ],

            'country' => [
                'type' => Type::string(),
                'description' => 'The country'
            ],

        ];
    }
}
