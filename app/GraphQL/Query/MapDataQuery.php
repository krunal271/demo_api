<?php

namespace App\GraphQL\Query;

use App\Core\Repository\ApiRepository;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class MapDataQuery extends Query
{

    public function __construct()
    {
        $this->mapData = new ApiRepository;

    }

    protected $attributes = [
        'name' => 'mapDataRecord',
        'description' => 'data to get map records'
    ];

    public function type(): Type
    {
        return GraphQL::type('mapDataRecord');
    }

    /**
     * Arguments
     *
     * Filter parameter
     * @return array
     */
    public function args(): array
    {
        return [
            'source_city_id' => [
                'name' => 'source_city_id',
                'type' => Type::int(),
                'rules' => ['required']
            ],
            'destination_city_id' => [
                'name' => 'destination_city_id',
                'type' => Type::int(),
                'rules' => ['required']
            ]
        ];
    }

    /**
     * returns the response of map data
     * @param $root
     * @param $args
     * @return mixed
     */
    public function resolve($root, $args)
    {
        $origin_city = $args['source_city_id'];
        $destination_city = $args['destination_city_id'];

        $data = $this->mapData->mapData($origin_city, $destination_city);
        return $data;
    }

    /**
     * Validation messages
     *
     * @param array $args
     * @return array
     */
    public function validationErrorMessages(array $args = []): array
    {
        return [
            'source_city_id.required' => 'Source city required.',
            'destination_city_id.required' => 'Destination city required.',
        ];
    }
}
