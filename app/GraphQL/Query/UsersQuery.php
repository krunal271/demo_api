<?php

namespace App\GraphQL\Query;

use App\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UsersQuery extends Query
{
    protected $attributes = [
        'name' => 'Users Query',
        'description' => 'A query of users'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('users'));
    }

    /**
     * filter parameters
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
               // 'rules' => ['required', 'name'] /* validation for request params */
            ],
            'like' => [
                'name' => 'like',
                'type' => Type::string()
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int()
            ],
            'page' => [
                'name' => 'page',
                'type' => Type::int()
            ],
        ];
    }

    /**
     * This function is for modify as per our requirement
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return User|User[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function resolve($root, $args, SelectFields $fields)
    {

        $where = function ($query) use ($args) {
            if (isset($args['id'])) {
                $query->where('id', $args['id']);
            }

            if (isset($args['name'])) {
                $query->where('name', $args['name']);
            }

            if (isset($args['like'])) {
                $query->where('name', 'like', '%' . $args['like'] . '%');
            }

        };
        $user = User::with(array_keys($fields->getRelations()))
            ->where($where)
            ->select($fields->getSelect());
            if (isset ($args['limit']) &&  (isset($args['page']))){
                $user =  $user->paginate($args['limit'], ['*'], 'page', $args['page']);
            }else{
                $user = $user->get();
            }

        return $user;
    }
}
