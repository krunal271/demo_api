<?php

namespace App\GraphQL\Query;

use App\City;
use App\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class CityQuery extends Query
{
    protected $attributes = [
        'name' => 'City Query',
        'description' => 'A query of cities'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('cities'));
    }

    /**
     * Filter parameter
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string()
            ],
            'like' => [
                'name' => 'like',
                'type' => Type::string()
            ],
        ];
    }

    /**
     * This function use for modify response as per our requirement
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return City[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        $where = function ($query) use ($args) {
            if (isset($args['id'])) {
                $query->where('id',$args['id']);
            }
            if (isset($args['like'])) {
                $query->where('name', 'like', '%' . $args['like'] . '%');
            }
        };
        $city = City::select('id as code','description','name')->where($where)->get();
        return $city;
    }
}
