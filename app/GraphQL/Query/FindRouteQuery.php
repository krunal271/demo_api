<?php

namespace App\GraphQL\Query;

use App\Core\Repository\ApiRepository;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class FindRouteQuery extends Query
{

    public function __construct()
    {
        $this->mapData = new ApiRepository;
    }

    protected $attributes = [
        'name' => 'findRoutesRecord',
        'description' => 'A query of find routes'
    ];

    public function type(): Type
    {
        return GraphQL::type('findRoutesRecord');
    }

    /**
     * Arguments
     *
     * Filter parameter
     * @return array
     */
    public function args(): array
    {
        return [
            'source_city_id' => [
                'name' => 'source_city_id',
                'type' => Type::int(),
                'rules' => ['required']
            ],
            'destination_city_id' => [
                'name' => 'destination_city_id',
                'type' => Type::int(),
                'rules' => ['required']
            ]
        ];
    }

    /**
     * This function use for modify response as per our requirement
     * @param $root
     * @param $args
     * @param SelectFields $fields
     * @return City[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function resolve($root, $args)
    {
        $origin_city = $args['source_city_id'];
        $destination_city = $args['destination_city_id'];

        $data = $this->mapData->findRoutes($origin_city, $destination_city);
        return $data;
    }

    public function validationErrorMessages(array $args = []): array
    {
        return [
            'source_city.required' => 'Source City Required.',
            'destination_city.required' => 'Destination City Required.',
        ];
    }

}
