<?php

namespace App\Exceptions;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ValidationExceptions
{
    public function validationException($request, $exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            return response()->json([
                'message' => config('exception.model_not_found'),
                'code' => 400
            ], Controller::$VALIDATION_FAILED_HTTP_CODE);
        }
        if ($exception instanceof NotFoundHttpException) {
            return response()->json([
                'message' => config('exception.url_not_found'),
                'code' => 400
            ], Controller::$VALIDATION_FAILED_HTTP_CODE);
        }

        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            return response()->json([
                'message' => $exception->validator->errors()->first(),
                'code' => 400
            ], Controller::$VALIDATION_FAILED_HTTP_CODE);
        }

        if ($exception instanceof code) {
            return response()->json([
                'message' => config('exception.unauthorized_user'),
                'code' => 401
            ], Controller::$UNAUTHORIZED_USER);
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json([
                'message' => config('exception.method_not_found'),
                'code' => 400
            ], Controller::$VALIDATION_FAILED_HTTP_CODE);           
        }
        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            return response()->json([
                'message' => $exception->validator->errors()->first(),
                'code' => 400
            ], Controller::$VALIDATION_FAILED_HTTP_CODE);
        }

        if ($exception instanceof ClientException){
            return response()->json([
                'message' => $exception->getResponse()->getBody(),
                'status' => $exception->getCode(),
            ],Controller::$VALIDATION_FAILED_HTTP_CODE);
        }

        if ($exception instanceof AuthenticationException){
            return response()->json([
                'message' => 'Unauthorized user.',
                'status' => 401
            ],Controller::$UNAUTHORIZED_USER);
        }
        return parent::render($request, $exception);
    }
}