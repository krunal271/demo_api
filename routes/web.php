<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router = app('Dingo\Api\Routing\Router');

$router->version('v1',function ($router){

    $router->post('login', [
        'as' => 'login', 'uses' => 'App\Http\Controllers\AuthController@loginCmsUser'
    ]);
    $router->get('/key', function() {
        return str_random(32);
    });
    /* customer */
    $router->get('customer-list', [
        'as' => 'customer-list', 'uses' => 'App\Http\Controllers\CustomerController@customerList'
    ]);
    $router->get('view-customer', [
        'as' => 'view-customer', 'uses' => 'App\Http\Controllers\CustomerController@viewCustomer'
    ]);
    $router->post('add-customer', [
        'as' => 'add-customer', 'uses' => 'App\Http\Controllers\CustomerController@addCustomer'
    ]);
    $router->post('edit-customer', [
        'as' => 'edit-customer', 'uses' => 'App\Http\Controllers\CustomerController@editCustomer'
    ]);
    //routes only for( licensee_administrator )
    $router->group(['namespace' => 'App\Http\Controllers','middleware' =>'auth',
        ['role:licensee_administrator']], function () use ($router){
        $router->get('get-all-roles', [
            'as' => 'get-all-roles', 'uses' => 'UserManagementController@getAllRoles'
        ]);
        $router->post('add-cms-user', [
            'as' => 'add-cms-user', 'uses' => 'UserManagementController@addCmsUser'
        ]);
        $router->post('edit-cms-user', [
            'as' => 'edit-cms-user', 'uses' => 'UserManagementController@editCmsUser'
        ]);
        $router->get('roles-permissions-list', [
            'as' => 'roles-permissions-list', 'uses' => 'UserManagementController@rolesPermissionsList'
        ]);
        $router->get('permissions-list', [
            'as' => 'permissions-list', 'uses' => 'UserManagementController@permissionsList'
        ]);
    });

    //routes for( brand_administrator, licensee_administrator )
    $router->group(['namespace' => 'App\Http\Controllers','middleware' =>'auth',
        ['role:brand_administrator|licensee_administrator']], function () use ($router){
        $router->post('cms-user-list', [
            'as' => 'cms-user-list', 'uses' => 'UserManagementController@cmsUserList'
        ]);
        $router->post('add-licensee', [
            'as' => 'add-licensee', 'uses' => 'GeneralController@addLicenseeDetail'
        ]);
        $router->post('edit-licensee', [
            'as' => 'edit-licensee', 'uses' => 'GeneralController@editLicenseeDetail'
        ]);
        $router->post('cms-user-detail', [
            'as' => 'cms-user-detail', 'uses' => 'UserManagementController@cmsUserDetail'
        ]);
    });

    //routes for( product_manager, brand_administrator, licensee_administrator )
    $router->group(['namespace' => 'App\Http\Controllers','middleware' =>'auth',
        ['role:product_manager|brand_administrator|licensee_administrator']], function () use ($router){

        $router->post('task-list', [
            'as' => 'task-list', 'uses' => 'TaskController@getTaskList'
        ]);

        $router->post('task-add', [
            'as' => 'add-task-data', 'uses' => 'TaskController@addTaskData'
        ]);
        $router->post('task-edit', [
            'as' => 'edit-task-status', 'uses' => 'TaskController@editTask'
        ]);
        $router->post('task-add-complete', [
            'as' => 'task-add-complete', 'uses' => 'TaskController@addToComplete'
        ]);

        $router->post('task-delete', [
            'as' => 'task-delete', 'uses' => 'TaskController@deleteTask'
        ]);

        $router->post('task-add-snooze', [
            'as' => 'task-add-snooze', 'uses' => 'TaskController@addtoSnooze'
        ]);
        $router->post('add-account-notes', [
            'as' => 'add-account-notes', 'uses' => 'AccountNotesController@addAccountNotes'
        ]);
        $router->post('edit-account-notes', [
            'as' => 'edit-account-notes', 'uses' => 'AccountNotesController@editAccountNotes'
        ]);
        $router->get('sales-representative-list', [
            'as' => 'sales-representative-list', 'uses' => 'AccountNotesController@salesRepresentativeList'
        ]);
    });

    //routes for( consultant, agent, product_manager, brand_administrator, licensee_administrator )
    $router->group(['namespace' => 'App\Http\Controllers','middleware' =>'auth',
        ['role:consultant|agent|product_manager|brand_administrator|licensee_administrator']], function () use ($router){
        $router->get('licensee-list', [
            'as' => 'licensee-list', 'uses' => 'GeneralController@licenseeList'
        ]);
        $router->post('licensee-detail', [
            'as' => 'licensee-detail', 'uses' => 'GeneralController@licenseeDetail'
        ]);
        $router->get('template-list', [
            'as' => 'template-list', 'uses' => 'FrontendConfigurationController@templateList'
        ]);
        $router->get('get-products', [
            'as' => 'get-products', 'uses' => 'ApiController@getProducts'
        ]);
        $router->post('add-frontend-configuration', [
            'as' => 'add-frontend-configuration', 'uses' => 'FrontendConfigurationController@addFrontendConfiguration'
        ]);
        $router->post('edit-frontend-configuration', [
            'as' => 'edit-frontend-configuration', 'uses' => 'FrontendConfigurationController@editFrontendConfiguration'
        ]);
        $router->post('frontend-configuration-detail', [
            'as' => 'frontend-configuration-detail', 'uses' => 'FrontendConfigurationController@frontendConfigurationDetail'
        ]);
        $router->get('backend-services', [
            'as' => 'backend-services', 'uses' => 'BackendConfigurationController@backendServices'
        ]);
        $router->post('add-backend-configuration', [
            'as' => 'add-backend-configuration', 'uses' => 'BackendConfigurationController@addBackendConfiguration'
        ]);
        $router->post('edit-backend-configuration', [
            'as' => 'edit-backend-configuration', 'uses' => 'BackendConfigurationController@editBackendConfiguration'
        ]);
        $router->post('backend-configuration-detail', [
            'as' => 'backend-configuration-detail', 'uses' => 'BackendConfigurationController@backendConfigurationDetail'
        ]);
        $router->post('add-geo-data', [
            'as' => 'add-geo-data', 'uses' => 'GeoDataController@addGeoData'
        ]);
        $router->post('edit-geo-data', [
            'as' => 'edit-geo-data', 'uses' => 'GeoDataController@editGeoData'
        ]);
        $router->post('view-geo-data', [
            'as' => 'view-geo-data', 'uses' => 'GeoDataController@viewGeoData'
        ]);
        $router->post('add-inventory-market-place', [
            'as' => 'add-inventory-market-place', 'uses' => 'InventoryMarketPlaceController@addInventoryMarketPlace'
        ]);
        $router->post('edit-inventory-market-place', [
            'as' => 'edit-inventory-market-place', 'uses' => 'InventoryMarketPlaceController@editInventoryMarketPlace'
        ]);
        $router->get('view-inventory-market-place', [
            'as' => 'view-inventory-market-place', 'uses' => 'InventoryMarketPlaceController@viewInventoryMarketPlace'
        ]);
        $router->post('view-account-configuration', [
            'as' => 'view-account-configuration', 'uses' => 'AccountConfigurationController@viewAccountConfiguration'
        ]);
        $router->post('add-account-configuration', [
            'as' => 'add-account-configuration', 'uses' => 'AccountConfigurationController@addAccountConfiguration'
        ]);
        $router->post('edit-account-configuration', [
            'as' => 'edit-account-configuration', 'uses' => 'AccountConfigurationController@editAccountConfiguration'
        ]);
    });

    $router->group(['namespace' => 'App\Http\Controllers'], function () use ($router){

        $router->get('currencies', [
            'as' => 'currencies', 'uses' => 'ApiController@currencies'
        ]);
        $router->get('cities', [
        'as' => 'cities', 'uses' => 'ApiController@cities'
        ]);
    });

    //Routes for tour section
    $router->get('store-tours/{provider}', [
        'as' => 'store-tours', 'uses' => 'App\Http\Controllers\ToursController@storeTours'
    ]);
    $router->post('tour-details', [
        'as' => 'tour-details', 'uses' => 'App\Http\Controllers\ToursController@tour_details'
    ]);
    $router->get('tour-departure', [
        'as' => 'tour-departure', 'uses' => 'App\Http\Controllers\ToursController@getTourDepartures'
    ]);
    $router->post('get-departures', [
        'as' => 'get-departures', 'uses' => 'App\Http\Controllers\ToursController@getTourDepartures'
    ]);
    $router->post('search-tours', [
        'as' => 'search-tours', 'uses' => 'App\Http\Controllers\ToursController@search_tours'
    ]);
    

   //routes for (all auth user)
   // ,'middleware' =>'auth'
   $router->group(['namespace' => 'App\Http\Controllers'], function () use ($router){
       $router->get('/', function () use ($router) {
           return $router->app->version();
       });
       $router->post('customers', [
          'as' => 'customers', 'uses' => 'CustomerController@customers'
        ]);
        $router->post('get-customer', [
          'as' => 'get-customer', 'uses' => 'CustomerController@getCustomer'
        ]);
       $router->get('prefences', [
           'as' => 'prefences', 'uses' => 'ApiController@prefences'
       ]);
        $router->post('save-itinerary', [
          'as' => 'save-itinerary', 'uses' => 'ItineraryController@saveItinerary'
        ]);
        $router->get('get-saved-itineraries', [
          'as' => 'get-saved-itineraries', 'uses' => 'ItineraryController@getSavedItinerary'
        ]);
        $router->post('get-pending-booking', [
          'as' => 'get-pending-booking', 'uses' => 'ItineraryController@getPendingBooking'
        ]);
       $router->post('auto-routes', [
           'as' => 'auto-routes', 'uses' => 'ApiController@autoRoutes'
       ]);
       $router->get('auto-routes-get', [
           'as' => 'auto-routes', 'uses' => 'ApiController@autoRoutesGet'
       ]);
       $router->get('city-cache', [
           'as' => 'city-cache', 'uses' => 'ApiController@cityCache'
       ]);
       $router->post('prefetch-hotels', [
           'as' => 'prefetch-hotels', 'uses' => 'ApiController@prefetchHotels'
       ]);
       $router->post('prefetch-activity', [
           'as' => 'prefetch-activity', 'uses' => 'ActivityController@prefetchActivity'
       ]);
       $router->get('prefetch-activity', [
           'as' => 'prefetch-activity', 'uses' => 'ActivityController@prefetchActivity'
       ]);
       $router->post('activity-list', [
           'as' => 'activity-list', 'uses' => 'ActivityController@listActivities'
       ]);
       $router->post('product-list', [
           'as' => 'product-list', 'uses' => 'ProductController@getProductsFromFile'
       ]);
       $router->post('prefetch-transport', [
           'as' => 'prefetch-transport', 'uses' => 'TransportController@searchTransport'
       ]);
       $router->post('search-hotels', [
           'as' => 'search-hotels', 'uses' => 'HotelController@searchHotels'
       ]);
       $router->post('search-transport', [
           'as' => 'search-transport', 'uses' => 'TransportController@searchTransport'
       ]);
       $router->post('transport-list', [
        'as' => 'transport-list', 'uses' => 'TransportController@listTransport'
       ]);
       $router->post('map-data', [
           'as' => 'map-data', 'uses' => 'ApiController@mapData'
       ]);
       $router->get('auto-route-get', [
           'as' => 'auto-route-get', 'uses' => 'ApiController@autoRouteGet'
       ]);
       $router->get('get-all-cities', [
           'as' => 'get-all-cities', 'uses' => 'ApiController@getAllCities'
       ]);
       $router->get('map-viator-dest', [
           'as' => 'map-viator-dest', 'uses' => 'ApiController@mapViatorData'
       ]);
       $router->post('cities-info', [
           'as' => 'cities-info', 'uses' => 'ApiController@cityInfo'
       ]);
       $router->get('cities-feed', [
           'as' => 'cities-feed', 'uses' => 'FeedController@cityFeed'
       ]);
       $router->get('countries-feed', [
           'as' => 'countries-feed', 'uses' => 'FeedController@countryFeed'
       ]);
       $router->post('nearest-cities', [
           'as' => 'nearest-cities', 'uses' => 'ApiController@nearestCities'
       ]);
       $router->get('timezones', [
           'as' => 'timezones', 'uses' => 'ApiController@timezones'
       ]);
       $router->get('countries', [
           'as' => 'countries', 'uses' => 'ApiController@countries'
       ]);
       $router->get('pre-set-account-options', [
           'as' => 'pre-set-account-options', 'uses' => 'GeneralController@preSetAccountOptionList'
       ]);
       $router->post('find-routes', [
           'as' => 'find-routes', 'uses' => 'ApiController@findRoutes'
       ]);
       $router->get('get-oleryId-from-expediaId', [
           'as' => 'get-oleryId-from-expediaId', 'uses' => 'OleryMappingController@getOleryIdFromExpediaId'
       ]);
       $router->post('hotel-details', [
           'as' => 'hotel-detail', 'uses' => 'HotelController@hotelDetails'
       ]);
       $router->post('get-city', [
           'as' => 'get-city', 'uses' => 'ApiController@getCityById'
       ]);
        //    $router->post('search-tours', [
        //        'as' => 'search-tours', 'uses' => 'ToursController@search_tours'
        //    ]);
       
        //    $router->post('tour-details', [
        //        'as' => 'tour-details', 'uses' => 'ToursController@tour_details'
        //    ]);
        //    $router->get('tour-departure', [
        //        'as' => 'tour-departure', 'uses' => 'ToursController@getTourDepartures'
        //    ]);
        // $router->get('store-tours/{provider}', [
        //     'as' => 'store-tours', 'uses' => 'ToursController@storeTours'
        // ]);
        // $router->post('get-departures', [
        //     'as' => 'get-departures', 'uses' => 'ToursController@getTourDepartures'
        // ]);

        $router->post('viator-activity-detail', [
           'as' => 'viator-activity-detail', 'uses' => 'ActivityController@viatorActivityDetail'
        ]);

        $router->post('viator-hotel-list', [
           'as' => 'viator-hotel-list', 'uses' => 'ActivityController@getViatorHotelList'
        ]);
        
        $router->get('voyeger-cities-feed', [
           'as' => 'viator-activity-detail', 'uses' => 'FeedController@callReadCityCSV'
        ]);

        $router->get('voyeger-hotels-feed', [
           'as' => 'voyeger-hotels-feed', 'uses' => 'FeedController@callStoreHotels'
        ]);      

        $router->post('voyeger-get-hotels', [
           'as' => 'voyeger-get-hotels', 'uses' => 'VoyegerController@searchHotels'
        ]);   

        $router->post('voyeger-get-hotel-detail', [
           'as' => 'voyeger-get-hotel-detail', 'uses' => 'VoyegerController@voyegerHotelDetail'
        ]); 
   });
});
